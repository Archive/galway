;;;;;;;;;;;;;;;;;;;;;;;;;file.menu;;;;;;;;;;;;;;;;;;;;;
(define lang:file "Archivo")
(define lang:newfile "Nuevo Archivo")
(define lang:openfile "Abrir Archivo")
(define lang:savefile "Salvar Archivo")
(define lang:savefileas "Salvar Archivo Como...")
(define lang:print "Imprimir")
(define lang:ftp-client "Cliente de FTP")
(define lang:close    "Cerrar")
(define lang:exit "Salir")
(define lang:parse-as-html "Hacer parseo como HTML")
(define lang:parse-as-vrml "Hacer parseo como VRML")
(define lang:parse-as-scheme "Hacer parseo como Scheme")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Edit menu;;;;;;;;;;;;;;;;;;;;
(define lang:edit "Editar")
(define lang:copy "Copiar CTRL+C")
(define lang:cut "Cortar CTRL+X")
(define lang:paste "Pegar CTRL+P")
(define lang:finder "Buscar")
(define lang:selectall "Seleccionar Todo")
;;;;;;;;;;;;;;;;;;;;;;;;;Find dialog;;;;;;;;;;;;;;;;;;;;
(define lang:search "Buscar?")
(define lang:startB "Buscar desde el inicio?")
(define lang:startC "Iniciar en la posicion actual?")
(define lang:casesS "Sensible a mayusculas?")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;buttons;;;;;;;;;;;;;;;
(define lang:accept "OK")
(define lang:decline "Cancelar")
(define lang:publish "Publicar")
;;;;;;;;;;;;;;;;;;;;;;;;;;;Options menu;;;;;;;;;;;;;;;;;;;
(define lang:options "Opciones")
(define lang:uppertmp "Template Superior")
(define lang:lowertmp "Template Inferior")
(define lang:servpub  "Publicar al servidor")
(define lang:netscape "Preview en Netscape")
(define lang:freewrl "Preview VRML en FreeWrl")
;;;;;;;;;;;;;;;;;;;;;;;;;;;HTML;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define lang:html "HTML")
(define lang:lists "Listas")
(define lang:olists "Listas Ordenadas")
(define lang:ulists "Listas")
(define lang:separators "Separadores")
(define lang:paragraph "P�rrafo")
(define lang:horrule   "Regla Horizontal")
(define lang:breakline "Salto de l�nea")
(define lang:headers "Encabezados")
(define lang:links "Enlaces")
(define lang:forms "Formas")
(define lang:h-form  "Form")
(define lang:h-labels "Label")
(define lang:h-texts "Texto")
(define lang:h-paswd* "Password")
(define lang:h-radio "Radio")
(define lang:h-send "Enviar")
(define lang:h-delete "Borrar")
(define lang:h-fimage "Imagen")
(define lang:h-button "Bot�n")
(define lang:h-menu "Men�")
(define lang:h-element "Elemento")
(define lang:image "Tag de Imagen")
(define lang:image-dialog "Dialogo para Imagenes")
(define lang:tables "Tablas")
(define lang:newtag "Nuevo Tag")
(define lang:types "Tipos")
(define lang:bold "Negrita")
(define lang:italic "It�lica")
(define lang:pre "Preformateado")
(define lang:tt "Teletipo")
(define lang:cite "Cita")
(define lang:dir* "Dir")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Scripts;;;;;;;;;;;;;;;;;;;;
(define lang:scripts "Scripts")
(define lang:enunciados "Enunciados")
(define lang:condition "If...")
(define lang:iteration "for...")
(define lang:function "Function")
(define lang:variable "Var")
(define lang:whilst "while")
;;;;;;;;;;;;;;;;;;;;preferences;;;;;;;;;;;;;;;;;;;;;
(define lang:english "Ingles")
(define lang:spanish "Castellano")
(define lang:italian "Italiano")
(define lang:text-pref "Preferenciass del Texto:")
(define lang:langu "Idioma:")
(define lang:ro "Solo Lectura")
(define lang:word-wrap "Word-Wrap")
;;;;;;;;;;;;;;;;;;;;;ftp stuff;;;;;;;;;;;;;;;;;;;;;;;
(define lang:ftp-pub "Publicacin por ftp")
(define lang:server "Servidor:")
(define lang:login "login:")
(define lang:password "contrasena:")
(define lang:ldir "Dir Local")
(define lang:rdir "Dir Remoto")
(define lang:lfiles "Archivos Locales")
(define lang:rfiles "Archivos Remotos")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;dialogs;;;;;;;;;;;;;;;;;;;
(define lang:dialog-quit "Esta seguro que desea salir?")
(define lang:welcome (string-append "Bienvenido a galway 0.33.\n"
				    "Recuerde que esta es una version preliminar.\n"
				    "Mande reportes de errores o comentarios a jarios@usa.net"))










