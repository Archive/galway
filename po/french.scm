;;;;;;;;;;;;;;;;;;;;;;;;;file.menu;;;;;;;;;;;;;;;;;;;;;
(define lang:file "Document")
(define lang:newfile "Nouvelle Document")
(define lang:openfile "Ouvrir Document")
(define lang:savefile "Save File")
(define lang:savefileas "Save File as")
(define lang:print "Imprimer")
(define lang:ftp-client "Mini FTP client")
(define lang:close  "Fermer")
(define lang:exit "Sortir")
(define lang:parse-as-html "Parse as HTML")
(define lang:parse-as-vrml "Parse as VRML")
(define lang:parse-as-scheme "Parse as Scheme")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Edit menu;;;;;;;;;;;;;;;;;;;;
(define lang:edit "Editer")
(define lang:copy "Copier")
(define lang:cut "Couper")
(define lang:paste "Collage")
(define lang:finder "Chercher")
(define lang:selectall "Select All")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;find dialog;;;;;;;;;;;;;;;;;;
(define lang:search "Chercher?")
(define lang:startB "Start from beginning?")
(define lang:startC "Start at cursor?")
(define lang:caseS "Search at current point?")
;;;;;;;;;;;;;;;;;;;;;;;;;;buttons;;;;;;;;;;;;;;;;;;;;;;;
(define lang:accept "OK")
(define lang:decline "Cancel")
(define lang:publish "Publish")
;;;;;;;;;;;;;;;;;;;;;;;;;;;Options menu;;;;;;;;;;;;;;;;;;;
(define lang:options "Options")
(define lang:uppertmp "Upper Template")
(define lang:lowertmp "Lower Template")
(define lang:servpub  "Server Publish")
(define lang:netscape "Preview HTML in Netscape")
(define lang:freewrl "Preview VRML in Freewrl")
;;;;;;;;;;;;;;;;;;;;;;;;;;;HTML;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define lang:html "HTML")
(define lang:lists "Lists")
(define lang:olists "Ordered Lists")
(define lang:ulists "Unordered Lists")
(define lang:separators "Separators")
(define lang:paragraph "Paragraph")
(define lang:horrule   "Horizontal Rule")
(define lang:breakline "Break Line")
(define lang:headers "Headers")
(define lang:links "Links")
(define lang:forms "Forms")
(define lang:h-form  "Form")
(define lang:h-labels "Label")
(define lang:h-texts "texto")
(define lang:h-paswd* "Password")
(define lang:h-radio "Radio Button")
(define lang:h-send "Send Button")
(define lang:h-delete "Delete")
(define lang:image "Image Tag")
(define lang:image-dialog "Image dialog")
(define lang:h-button "Button")
(define lang:h-menu "Menu")
(define lang:h-element "Element")
(define lang:image "Images")
(define lang:tables "Tables")
(define lang:newtag "New Tag")
(define lang:types "Types")
(define lang:bold "Bold")
(define lang:italic "Italic")
(define lang:pre "Preformatted")
(define lang:tt "Teletype")
(define lang:cite "Cite")
(define lang:dir* "Dir")
;;;;;;;;;;;;;;;;;;;;;;;;jscript;;;;;;;;;;;;
(define lang:scripts "JavaScript")
(define lang:enunciados "Statements")
(define lang:condition "If...")
(define lang:iteration "for...")
(define lang:function "Function")
(define lang:variable "Var")
(define lang:whilst "while")
;;;;;;;;;;;;;;;;;;;;preferences;;;;;;;;;;;;;;;;;;;;;
(define lang:english "English")
(define lang:spanish "Spanish")
(define lang:italian "Italian")
(define lang:text-pref "Text Preferences:")
(define lang:langu "Language:")
(define lang:ro "Read-Only")
(define lang:word-wrap "Word-Wrap")
;;;;;;;;;;;;;;;;;;;;;ftp stuff;;;;;;;;;;;;;;;;;;;;;;;
(define lang:ftp-pub "Ftp Publishing")
(define lang:server "Server")
(define lang:login "login:")
(define lang:password "password")
(define lang:ldir "Local dir")
(define lang:rdir "Remote dir")
(define lang:lfiles "Local files")
(define lang:rfiles "Remote files")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;dialogs;;;;;;;;;;;;;;;
(define lang:dialog-quit "Are you sure you want to quit?")
(define lang:welcome (string-append "Welcome to galway 0.32.\n"
				    "Please remember this is an unstable version.\n"
				    "Send bug reports or comments to jarios@usa.net"))









