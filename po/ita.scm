;;;;;;;;;;;;;;;;;;;;;;;;;file.menu;;;;;;;;;;;;;;;;;;;;;
(define lang:file "Archivio")
(define lang:newfile "Archivio Novo")
(define lang:openfile "Aprire Archivio")
(define lang:savefile "Salvare Archivio")
(define lang:savefileas "Salvare Archivio come...")
(define lang:ftp-client "Mini FTP client")
(define lang:print "Imprimire")
(define lang:close    "Chiudi")
(define lang:exit "Uscire")
(define lang:parse-as-html "Fare \"parse\" come HTML")
(define lang:parse-as-vrml "Fare \"parse\" come VRML")
(define lang:parse-as-scheme "Fare \"parse\" come Scheme")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Edit menu;;;;;;;;;;;;;;;;;;;;
(define lang:edit "Edizione")
(define lang:copy "Copia CTRL+C")
(define lang:cut "Taglia CTRL+X")
(define lang:paste "Incolla CTRL+P")
(define lang:finder "Cerca CTRL+F")
(define lang:selectall "Seleziona tutto")
;;;;;;;;;;;;;;;;;;;find dialog
(define lang:search "Cercare?")
(define lang:startB "Cercare dall'inizio?")
(define lang:startC "Cercare dal cursore?")
(define lang:caseS "Case sensitive?")
;;;;;;;;;;;;;;;;;;;;;;buttons;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define lang:accept "OK")
(define lang:decline "Cancellare")
(define lang:publish "Publicare")
;;;;;;;;;;;;;;;;;;;;;;;;;;;Options menu;;;;;;;;;;;;;;;;;;;
(define lang:options "Opzioni")
(define lang:uppertmp "Template Superiore")
(define lang:lowertmp "Template Inferiore")
(define lang:servpub  "Publicare al servidor")
(define lang:netscape "Preview con Netscape")
(define lang:freewrl "Preview con Freewrl")
;;;;;;;;;;;;;;;;;;;;;;;;;;;HTML;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define lang:html "HTML")
(define lang:lists "Lista")
(define lang:olists "Lista Ordinata")
(define lang:ulists "Lista")
(define lang:separators "Separatore")
(define lang:paragraph "Paragrafo")
(define lang:horrule   "HR")
(define lang:breakline "Interrunzione della linea")
(define lang:headers "Intestazioni")
(define lang:links "Collegamenti")
(define lang:forms "Moduli")
(define lang:h-form  "Modulo")
(define lang:h-labels "Etichetta")
(define lang:h-texts "Testo")
(define lang:h-paswd* "Password")
(define lang:h-radio "Radio")
(define lang:h-send "Enviar")
(define lang:h-delete "Borrar")
(define lang:h-fimage "Imagen")
(define lang:h-button "Bot�n")
(define lang:h-menu "Men�")
(define lang:h-element "Elemento")
(define lang:image "Imaggini Tag")
(define lang:image-dialog "Imaggini dialog")
(define lang:tables "Tabella")       
(define lang:newtag "Novo Tag")
(define lang:types "Tipo")
(define lang:bold "Negretto")
(define lang:italic "Corsivo")
(define lang:pre "Preformattato")
(define lang:tt "Telescritto")
(define lang:cite "Citazione")
(define lang:dir* "Directory")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Scripts;;;;;;;;;;;;;;;;;;;;
(define lang:scripts "Script")
(define lang:enunciados "Enunciados")
(define lang:condition "If...")
(define lang:iteration "for...")
(define lang:function "Function")
(define lang:variable "Var")
(define lang:whilst "while")
;;;;;;;;;;;;;;;;;;;;preferences;;;;;;;;;;;;;;;;;;;;;
(define lang:english "Inglese")
(define lang:spanish "Spagnolo")
(define lang:italian "Italiano")
(define lang:text-pref "Preferenza dal testo:")
(define lang:langu "Lingua:")
(define lang:ro "Read-Only")
(define lang:word-wrap "Word-Wrap")
;;;;;;;;;;;;;;;;;;;;;ftp stuff;;;;;;;;;;;;;;;;;;;;;;;
(define lang:ftp-pub "Publicare per FTP")
(define lang:server "Servitore")
(define lang:login "login:")
(define lang:password "password")
(define lang:ldir "Local dir")
(define lang:rdir "Remote dir")
(define lang:lfiles "Local files")
(define lang:rfiles "Remote files")
;;;;;;;;;;;;;;;;;;;;;;;;dialogs;;;;;;;;;;;;;;;;;;;;;;
(define lang:dialog-quit "Are you sure you want to quit?")
(define lang:welcome (string-append "Benvenutto a galway 0.33\n"
				    "Please remember this is an unstable version.\n"
				    "Send bug reports or comments to jarios@usa.net"))






















