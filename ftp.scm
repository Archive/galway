;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@arcavia.com       ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; This file contain functions related to galway ftp support.

;; Please note that all the low level functions
;; are located on the ftplib.scm file.

;; galway-ftp-client-local-name
;; galway-ftp-client-remote-name
;; galway-ftp-client-local-index
;; galway-ftp-client-remote-index

;; (galway-ftp-publish xml arg)

;; (galway-ftp-client xml arg)
;; (galway-ftp-client-add-items-signal xml arg)
;; (galway-ftp-client-entry xml arg)
;; (galway-ftp-client-entry-signal xml arg)
;; (galway-ftp-client-mkdir xml arg)
;; (galway-ftp-client-dir xml arg)
;; (galway-ftp-get-ls arg)
;; (galway-ftp-client-lcd xml arg)
;; (galway-ftp-client-cd xml arg)
;; (galway-ftp-client-ldel xml arg)
;; (galway-ftp-client-del xml arg)
;; (galway-ftp-client-connect xml arg)
;; (galway-ftp-client-signal xml arg)

(load "ftplib.scm")


;; The global vars for keeping track of the elemetns of the current
;; and the remote directories.
;; Shall this be local variables?

(define-public galway-ftp-client-local-name #f)
(define-public galway-ftp-client-remote-name #f)

;; The global indexes

(define-public galway-ftp-client-local-index #f)
(define-public galway-ftp-client-remote-index #f)

;; The following function is in charge of
;; making the ftp current file publish

(define (galway-ftp-publish xml arg)
  
  (call-with-current-continuation
     (lambda (break)

       (if (not galway-document-index)
	   (break #f))

    ;; First we save the file
    
       (galway-document-save xml #f)

    ;; Now. The second step will be to create the ftp port 
    ;; If we can't make a connection we'll finish the program
    
       (let ((ftp* (ftplib:ftp galway-server galway-login galway-password)))

	 (if (null? ftp*)
	     (break #f))


	 ;; Finally we copy the file unto the server:

	  (let ((galway-out  (open-input-file (list-ref galway-document-file-name
						     galway-document-index))))


	    (ftplib:putfile ftp* (list-ref galway-document-file-name
					   galway-document-index)
			    galway-out)
	    (close ftp*)
	    (close-input-port galway-out)

	(break #t))))))

;; The following function is the main cheese for loading the
;; ftp client. This very same function will be used
;; by the connect button to connect to a different server than the one specified
;; on the preferences dialog. 

(define (galway-ftp-client xml arg)
  
  (let ((ftp* (ftplib:ftp galway-server galway-login galway-password))
	(xml (glade-xml-new xml-file "ftp-client1")))

  (galway-ftp-client-dir xml (list ftp* 1))
  (galway-ftp-client-dir xml (list ftp* 0))

  (galway-ftp-client-add-items-signal xml arg)
  (galway-ftp-client-signal xml ftp*)

  ;;debug functon shall be deleted in the future ;)
  ;;(close ftp*)

  (gtk-widget-show (glade-xml-get-widget xml "ftp-client1"))))

(define (galway-ftp-client-add-items-signal xml arg)
  
  (gtk-signal-connect (glade-xml-get-widget xml "clist3") "select_row"
		      (lambda (row col event)
			(set! galway-ftp-client-local-index row)))

  (gtk-signal-connect (glade-xml-get-widget xml "clist4") "select_row"
		      (lambda (row col event)
			(set! galway-ftp-client-remote-index row))))


(define (galway-ftp-client-entry xml arg)
  (let* ((xml2 (glade-xml-new xml-file "ftp-entry1"))
	 (label (case arg
		  ((0 1 2) "Name of directory:")
		  ((3 4) "Name of file to be deleted:")))
	 (action (case arg
		   ((0) galway-ftp-client-mkdir)
		   ((1 2) galway-ftp-client-dir)))
	 (param (if (zero? arg)
		    #t
		    #f)))
		    
	 ;; FIXME: we are still missing the ldel and del calls 
	 ;; FIXME2: we also need to pass special flags for certain functions
	 ;; like galway-ftp-client-dir or galway-ftp-client-dir

    (galway-ftp-client-entry-signal (list xml xml2) (list action param))
    
    (gtk-label-set-text (glade-xml-get-widget xml2 "label53") label)
    
    (gtk-widget-show-all (glade-xml-get-widget xml2 "ftp-entry1"))))

(define (galway-ftp-client-entry-signal xml arg)
  
  (let ((xml1 (car xml))
	(xml2 (cadr xml))
	(action (car arg))
	(param  (cadr arg)))
		

  (gtk-signal-connect (glade-xml-get-widget xml2 "button108") "clicked"
		      (lambda ()
			(if param
			    (arg xml1 0)
			    (arg xml1 (gtk-entry-get-text (glade-xml-get-widget xml2 "combo-entry18"))))
			(gtk-button-clicked (gtk-entry-get-text (glade-xml-get-widget xml2 "button110")))))

  (gtk-signal-connect (glade-xml-get-widget xml2 "button110") "clicked"
		      (lambda ()
			(gtk-widget-hide-all (glade-xml-get-widget xml2 "ftp-entry1"))))))


;; (galway-ftp-client-entry-signal xml arg)
;;label53 conbo-entry18 button108-110

;; We create a new remote dir with the following function:

(define (galway-ftp-client-mkdir xml arg)
  
  (call-with-current-continuation
   (lambda (break)

     (if (member? galway-ftp-client-remote-name (cadr arg))
	 (break #f))

     (let ((response (ftplib:make-directory (car arg) (cadr arg))))

    ;; If we did not correctly create the directory we
    ;; return the message. In a future, I hope a gnome
    ;; dialog will appear with this answer <FIXME>
    ;; If everything is right, then we append the new created item

       (cond 
	((not (car response)) (display response) (break #f))

	(else (gtk-clist-append (vector (cadr arg))) (break #t))) ))))

;; This is a little tricky function.
;; We are going to use this function to append the full list
;; of items. FOr this functon cadr arg can be:

;; 1 => takes the current local directory using galway-document-file-name
;; 0 => adds the current remote directory
;; str => Takes str as current directory

(define (galway-ftp-client-dir xml arg)

  (let ((ls (case (cadr arg)
	      ((1)  galway-document-file-name)
	      ((0)  (galway-ftp-client-get-ls (car arg)))
	      (else (galway-ctree-read-dir (cadr arg)))))

	(clist (glade-xml-get-widget xml (if (zero? (cadr arg))
					     "clist4"
					     "clist3"))))						    	 
  (letrec ((itemizer
	    (lambda (ls n)
	      (cond 
	       ((null? ls) n)
	       (else (gtk-clist-append clist (vector (car ls)))
		    (itemizer (cdr ls) (1+ n)))))))

    (itemizer ls 0)
    (gtk-widget-show-all clist))))

(define (galway-ftp-client-get-ls arg)

     (let* ((file-out ".galway.tmp")
	    (list-out (open-output-file file-out)))

       (ftplib:dir-list arg list-out)
       (close-output-port list-out)

       (let ((ls (galway-string->ls (txt->string-file file-out))))

       (delete-file file-out)

       ls)))

;;Missing dialogs for these functions =(

;; (define (galway-ftp-client-lcd xml arg))
;; (define (galway-ftp-client-cd xml arg))

(define (galway-ftp-client-signal xml arg)
  
  ;; put

  (gtk-signal-connect (glade-xml-get-widget xml "button75") "clicked"
		      (lambda ()
			(let* ((file (list-ref galway-ftp-client-local galway-ftp-client-index))
			       (out  (open-output-file file)))
			  
			(ftplib:putfile arg file out))))
  
  ;; get

  (gtk-signal-connect (glade-xml-get-widget xml "button76") "clicked"
		      (lambda ()
			(let* ((file (list-ref galway-ftp-client-remote galway-ftp-client-index))
			       (in  (open-input-file file)))
			  
			(ftplib:putfile arg file in))))

  ;; ascii

  (gtk-signal-connect (glade-xml-get-widget xml "button77") "clicked"
		      (lambda ()
			(ftplib:set-xfer-type arg 'ascii)))

  ;; binary

  (gtk-signal-connect (glade-xml-get-widget xml "button78") "clicked"
		      (lambda ()
			(ftplib:set-xfer-type arg 'image))) 

  ;; mput 

  ;; mget

  ;; mkdir

  (gtk-signal-connect (glade-xml-get-widget xml "button81") "clicked"
		     (lambda ()
		       (galway-ftp-client-entry xml 0)))
 
  ;; lcd
  
  (gtk-signal-connect (glade-xml-get-widget xml "button82") "clicked"
		      (lambda ()
			(galway-ftp-client-entry xml arg)))

  ;; cd
  
  (gtk-signal-connect (glade-xml-get-widget xml "button83") "clicked"
		      (lambda ()
			(galway-ftp-client-entry xml arg)))

  ;; ldel

  (gtk-signal-connect (glade-xml-get-widget xml "button84") "clicked"
		      (lambda ()
			(galway-ftp-client-entry xml arg)))

  ;; del

  (gtk-signal-connect (glade-xml-get-widget xml "button85") "clicked"
		      (lambda ()
			(galway-ftp-client-entry xml arg)))
  
  ;; connect

  (gtk-signal-connect (glade-xml-get-widget xml "button86") "clicked"
		      (lambda ()
			(galway-ftp-client-connect xml arg)))


  ;; bye

  (gtk-signal-connect (glade-xml-get-widget xml "button87") "clicked"
		      (lambda ()
			(close arg)
			(gtk-widget-destroy (glade-xml-get-widget xml
								   "ftp-client1")))))	      

;;label51-52
;clist3-4



	 
	 


