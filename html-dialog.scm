;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; This file contain function related to the 
;; dif'rent creation of html dialogs and their signals.

;; (galway-create-html-list xml n type) 
;; (galway-dialog-html-list-signal xml)
;; (galway-dialog-html-list xml arg)

;; (galway-create-html-table xml col row atrib)
;; (galway-create-html-table-attributes ls str)
;; (galway-dialog-html-table-signal xml)
;; (galway-dialog-html-table xml arg)

;; (galway-dialog-html-link-signal xml)
;; (galway-dialog-html-link xml arg)

;; (galway-dialog-html-image-signal xml)
;; (galway-dialog-html-image xml arg)

;; (galway-string-null? str)

;; (galway-dialog-html-signal-all xml)


;; List functions

(define (galway-create-html-list xml n type)
 
  (define text (glade-xml-get-widget xml "text1"))

  (letrec((do-list
	   (lambda (str k)
	     (cond
	      ((= k n) str)
	      (else (do-list (string-append str "<LI>\n") (1+ k)))))))

    (gtk-text-insert text #f "red" #f    
		     (string-append "<" type ">\n" (do-list "" 0) "</" type ">\n")
		     -1)

    (gtk-widget-grab-focus text) ))

;; We really don't need to have and arg. However we 
;; need that all function to be call receive two args.

(define (galway-dialog-html-list-signal xml)
  (define type "OL")
      
  (gtk-signal-connect (glade-xml-get-widget xml "button54") "clicked" 
		      (lambda()
			(gtk-button-clicked (glade-xml-get-widget xml "button55"))
			(gtk-button-clicked (glade-xml-get-widget xml "button56"))))


  (gtk-signal-connect (glade-xml-get-widget xml "button55") "clicked" 
		      (lambda()
			(galway-create-html-list xml 
						 (gtk-spin-button-get-value-as-int 
						  (glade-xml-get-widget xml "spinbutton7"))
						 type)))

  (gtk-signal-connect (glade-xml-get-widget xml "button56") "clicked" 
		      (lambda()
			(gtk-widget-hide-all (glade-xml-get-widget xml "list")))) 

  (gtk-signal-connect (glade-xml-get-widget xml "radiobutton3") "toggled"
		      (lambda()
			(set! type "OL")))
  
  (gtk-signal-connect (glade-xml-get-widget xml "radiobutton4") "toggled"
		      (lambda()
			(set! type "UL"))) )
   
;; The final functions for calling the lists:
;; arg stands for ordered or unordered 

(define (galway-dialog-html-list xml arg)

  (gtk-toggle-button-toggled (glade-xml-get-widget 
			      xml 
			      (if arg 
				  "radiobutton3"
				  "radiobutton4")))
      
  (gtk-widget-show-all (glade-xml-get-widget xml "list")))

;; Table Functions

(define (galway-create-html-table xml col row atrib)

  (define text (glade-xml-get-widget xml "text1"))

  (let ((table  (string-append "<TABLE" atrib ">\n"))
	(ctable "</TABLE>\n")
	(row-str "<TR>\n") 
	(crow-str "</TR>\n")
	(cell-str "<TD>  </TD>\n"))

    (letrec ((do-row 
	      (lambda (str n)
		(cond 
		 ((= n row) (string-append str crow-str))
		 (else (do-row (do-column (string-append str row-str) 0) (1+ n))))))
	     (do-column
	      (lambda (str n)
		(cond
		 ((= n col) str)
		 (else (do-column (string-append str cell-str) (1+ n)))))) )

      (gtk-text-insert text #f "red" #f 
		       (string-append (do-row table 0) ctable) -1)

      (gtk-widget-grab-focus text) )))



(define (galway-create-html-table-attributes ls str)
 
 (let ((border (if (= (car ls) 1) "" (number->string (car ls))))
	(cellp (if  (= (cadr ls) 1) ""  (number->string (cadr ls))))
	(cells (if (= (caddr ls) 1) ""  (number->string (caddr ls))))
	(twidth (if (= (cadddr ls) 1) ""  (number->string (cadddr ls)))) )

    (define (check? str type . opt)   
      (if (> (string-length str) 0) 
	  (string-append type str 
			 (if (null? opt) 
			     "" 
			     (car opt))) 
	  ""))

    (string-append (check? border " BORDER=") 
		   (check? cells " CELLSPACING=")
		   (check? cellp " CELLPADDING=") 
		   (check? twidth " WIDTH=" "%")
		   (check? str " BACKGROUND=")) ))
 

(define (galway-dialog-html-table-signal xml)
  (define galway-table-bg #t)

  (gtk-signal-connect (glade-xml-get-widget xml "button51") "clicked"
		      (lambda () 
			(gtk-button-clicked (glade-xml-get-widget xml "button52"))
			(gtk-button-clicked (glade-xml-get-widget xml "button53"))))
						       
  (gtk-signal-connect (glade-xml-get-widget xml "button52") "clicked"
		      (lambda()
			(galway-create-html-table 
			 xml
			 (gtk-spin-button-get-value-as-int (glade-xml-get-widget xml "spinbutton1"))
			 (gtk-spin-button-get-value-as-int (glade-xml-get-widget xml "spinbutton2"))
			 (galway-create-html-table-attributes (map
							       gtk-spin-button-get-value-as-int
							       (list (glade-xml-get-widget xml "spinbutton3")
								     (glade-xml-get-widget xml "spinbutton4")
								     (glade-xml-get-widget xml "spinbutton5")
								     (glade-xml-get-widget xml "spinbutton6")))
							      (gtk-entry-get-text 
							       (glade-xml-get-widget xml
										     (if galway-table-bg
											 "combo-entry4"
											 "combo-entry5"))))))) 

  (gtk-signal-connect (glade-xml-get-widget xml "button53") "clicked"
		      (lambda()
			(gtk-widget-hide-all (glade-xml-get-widget xml "table"))))

   (gtk-signal-connect (glade-xml-get-widget xml "radiobutton1") "toggled"
		      (lambda()
			(set! galway-table-bg #t)))
  
  (gtk-signal-connect (glade-xml-get-widget xml "radiobutton2") "toggled"
		      (lambda()
			(set! galway-table-bg #f))) )
  

(define (galway-dialog-html-table xml arg)
  (gtk-widget-show-all (glade-xml-get-widget xml "table")))

;; Link functions

(define (galway-dialog-html-link-signal xml)
  
  (gtk-signal-connect (glade-xml-get-widget xml "button58") "clicked"
		      (lambda ()
			(gtk-button-clicked (glade-xml-get-widget xml "button59"))
			(gtk-button-clicked (glade-xml-get-widget xml "button60"))))

  (gtk-signal-connect (glade-xml-get-widget xml "button59") "clicked"
		      (lambda ()
			(let ((text (glade-xml-get-widget xml "text1")))
			  (gtk-text-freeze text)
			  (gtk-text-insert text #f "red" #f
					   (string-append "<A HREF=\""
							  (gtk-entry-get-text (glade-xml-get-widget xml 
												    "combo-entry7"))
							  ">"
							  (gtk-entry-get-text (glade-xml-get-widget xml "entry5"))
							  "</A>")
					   -1)
			  (gtk-text-thaw text)
			  (gtk-widget-grab-focus text))))

  (gtk-signal-connect (glade-xml-get-widget xml "button60") "clicked"
		      (lambda ()
			(gtk-widget-hide-all (glade-xml-get-widget xml "links")))) )

(define (galway-dialog-html-link xml arg)
  (gtk-widget-show-all (glade-xml-get-widget xml "links")))
					
;; Image functions

(define (galway-dialog-html-image-signal xml)
  
  (gtk-signal-connect (glade-xml-get-widget xml "button61") "clicked"
		      (lambda()
			(gtk-button-clicked (glade-xml-get-widget xml  "button62"))
					      (gtk-button-clicked (glade-xml-get-widget xml "button63"))))
 
 (gtk-signal-connect (glade-xml-get-widget xml "button62") "clicked"
		      (lambda()
			(let* ((text (glade-xml-get-widget xml "text1"))
			       (img  (gtk-entry-get-text (glade-xml-get-widget xml "combo-entry8")))
			       (alt-text (gtk-entry-get-text (glade-xml-get-widget xml "entry8")))
			       (link (gtk-entry-get-text (glade-xml-get-widget xml "combo-entry9")))
			       (h (gtk-spin-button-get-value-as-int (glade-xml-get-widget xml "spinbutton8")))
			       (w  (gtk-spin-button-get-value-as-int (glade-xml-get-widget xml "spinbutton9")))
			       (height (if (not (zero?  h)) h #f))
			       (width (if (not (zero? w)) w #f))
			       (status (gtk-entry-get-text (glade-xml-get-widget xml "entry10"))))

			  (gtk-text-freeze text)
			  (gtk-text-insert text #f "red" #f
					   (string-append (if (galway-string-null? link)
							      ""
							      (string-append "<A HREF=\"" link ">"))
							  "<IMG SRC=\""
							  img 
							  "\""
							  (if (not width)
							      ""
							      (string-append "WIDTH=\"" 
									     (number->string width) "\" "))
							  (if (not height)
							      ""
							      (string-append "HEIGTH=\"" 
									     (number->string height) "\" "))
							  ">"
							  (if (galway-string-null? link)
							      ""
							      "</A>"))
					   -1)			   
			  (gtk-text-thaw text)
			  (gtk-widget-grab-focus text)
			  (gtk-widget-hide-all (glade-xml-get-widget xml "images")))))

(gtk-signal-connect (glade-xml-get-widget xml "button63") "clicked"
					  (lambda()
					    (gtk-widget-hide-all (glade-xml-get-widget xml "images")))) )

(define (galway-dialog-html-image xml arg)
  (gtk-widget-show-all (glade-xml-get-widget xml "images")))
  
;; This function should not be here...

(define (galway-string-null? str)
  (if (zero? (string-length str))
      #f
      #t))
    
;; The big function that will handle all the signals...

(define-public (galway-dialog-html-signal-all xml)

  (galway-dialog-html-list-signal xml)
  (galway-dialog-html-table-signal xml)
  (galway-dialog-html-link-signal xml)
  (galway-dialog-html-image-signal xml))













