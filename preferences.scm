;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Functions and variables for managing preferences.

;; (galway-check-rc xml)
;; (galway-write-rc xml)

;; (galway-dialog-preferences-signal xml)
;; (galway-dialog-preferences xml arg)

(define (galway-check-rc xml)
  
  (let ((galwayrc (string-append (getenv "HOME") "/.galwayrc")))
    
    (cond
     ((access? galwayrc R_OK)
      (load galwayrc)
      (include "templates.scm")
  
      (map (lambda (ls var) 
	     (gtk-entry-set-text (glade-xml-get-widget xml ls) var)) 
	   (list "entry18" "entry19" "entry20" 
		 "combo-entry16" "combo-entry17" "entry23"
		 "combo-entry19" "combo-entry20")
	   (list galway-user galway-company galway-email 
		 galway-server galway-login galway-password
		 galway-start-dir galway-project-dir )))
     
     (galway-plugin-load xml galway-plugin-list)


     (else  #f))))

(define (galway-write-rc xml)

  (let* ((open-def "(define-public")
	 (file  (string-append (getenv "HOME") "/.galwayrc"))
	 (out (open-output-file file)))
    
    (letrec ((stringer
	      (lambda (ls entry str)
		(cond
		 ((null? ls) str)
		 (else (stringer (cdr ls) (cdr entry)
			   (string-append str 
					  open-def (car ls) "\"" 
					  (gtk-entry-get-text (glade-xml-get-widget xml (car entry)))
					  "\")\n")))))) )
	

      (let* ((str (stringer (list " galway-user " " galway-company "  " galway-email " 
			      " galway-server "  " galway-login " " galway-password "
			       " galway-start-dir " " galway-project-dir " )
			    (list "entry18" "entry19" "entry20"
			      "combo-entry16" "combo-entry17" "entry23"
			      "combo-entry19" "combo-entry20")
			    ""))
	    (n (string-length str)))

	;; FIXME: Reduce the number of calls

	(for-each (lambda (c) (write-char c out)) (string->list "(define galway-plugin-list '"))
	(write galway-plugin-list out)
	(for-each (lambda (c) (write-char c out)) (string->list ")\n"))
	(for-each (lambda (c) (write-char c out)) (string->list str))
       
	(close-output-port out)
	(load file)
	(include "templates.scm") ))))

;; THe machinery to create the .galwayrc files. 
;; Each time we choose ok/apply this file is generated.

(define (galway-dialog-preferences-signal xml)
  (gtk-signal-connect (glade-xml-get-widget xml "button99") "clicked"
		      (lambda () (for-each (lambda(x)(gtk-button-clicked (glade-xml-get-widget xml x))) '("button100" "button101"))))

  (gtk-signal-connect (glade-xml-get-widget xml "button100") "clicked" (lambda () (galway-write-rc xml)))
		      

  (gtk-signal-connect (glade-xml-get-widget xml "button101") "clicked"
		      (lambda () (gtk-widget-hide-all (glade-xml-get-widget xml "preferences1"))))

  (galway-plugin-dialog-signal xml))


(define (galway-dialog-preferences xml arg) (gtk-widget-show-all (glade-xml-get-widget xml "preferences1")))



	






