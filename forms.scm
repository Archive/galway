;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios O.                     ;;
;; jarios@usa.net                    ;;
;; http://erin.linuxbox.com          ;;
;; Galway v0.4.0                     ;;
;; Diciembre 1998                    ;;
;; Junio 1999                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;Licencia;;;;;;;;;;;;;;;;;;;
;;GPL                                            ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;forms.s;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (fform text str)
 (gtk-widget-realize text)
 (gtk-text-insert text #f "red" #f str -1)
 (gtk-text-thaw text)
 (gtk-widget-grab-focus text))

(define (form text) (fform text "<FORM action=\" \" method=\"  \"> \n \n </FORM>"))
    
(define (flabel text) (fform text "<LABEL for=\" \" > \n </LABEL>" ))
    
(define (ftext text) (fform text "<TEXTAREA name=\" \" rows=\" \" cols=\" \"> \n </TEXTAREA>"))
   
(define (finput type text)
  (let ((str "<INPUT type=\" ")        
	(name " name=\" \"")       
	(value " value=\" "))   
    
    ;; type 0 = text   4 = borrar   8 = check
    ;; type 1 = pwd    5 = im�gen   9 = menu
    ;; type 2 = opcion 6 = bot�n   10 = element 
    ;; type 3 = enviar 7 = radio


    (gtk-widget-realize text) 
    (gtk-text-insert text #f "red" #f 
		     (case type
		       ((0 1) (string-append str "text\"" name ">"))
		       ((3)   (string-append str "submit\"" value "Send\">"))
		       ((4)   (string-append str "reset\"" value "Borrar\">"))
		       ((5)   (string-append str "image\" " "src =\" \">"))
		       ((6)   (string-append str "button\"" value " \">"))
		       ((7)   (string-append str "radio\"" name value "\" >")) 
		       ((8 9)   (string-append str "check\"" name value "\" >")))
		     -1)
    (gtk-text-thaw text)
    (gtk-widget-grab-focus text)  ))


(define (fselect text) (fform text "<SELECT name=\"  \"  size =\" \" > \n \n</SELECT>"))
   
(define (foption text) (fform text "<OPTION>\n \n </OPTION>"))



