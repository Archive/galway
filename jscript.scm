;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; This file contains definitions for jscript functions.

;; Sfunc, Sif, Sfor, Swhile, Svar,
;; Sbreak, Scontinue, Sreturn, Swith

(define Sfunc "function name(){\n\n }")

(define Sif  "if( ){\n\n} \n else{\n\n}")
  
(define Sfor "for(var i=0;i<n;i++){\n \n}")

(define Swhile "while( ){\n \n}")

(define Svar "var nombre = 0;")

(define Sbreak "break;")

(define Scontinue "continue;")

(define Sreturn "return exp;")

(define Swith "with(object){\nstatements...\n")

