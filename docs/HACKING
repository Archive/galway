
Hacking on Galway *
-------------------

  Galway code was created with the following ideas:

	- code has to be maintainable.
	- code has to be good.
	- code has to be extensible.
	- code has to make sense.
        - code has to be functional.
        - code has to be short(in size).
 
    When you submit code to me for inclusion in Galway please remember
    these points and please follow the coding style used in the programme.
    

The Galway coding style.
--------------------------

The coding style I use is a mix of various Scheme styles. Always keep in
mind the following points:

-If you create a new file please follow the standard header used on all
 the scheme files:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; ariel@arcavia.com        ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Functions and variable for multi file management.

;; (galway-sample-function xml arg)

The first part shall contain your name and contact information.
The second part, is the copyleft notice. Do not include any other
license for I will not accept it.
The third part is a one liner describing the content of the file.
Finally, you should list the 'prototypes of all the functions created.

-Use (define foo a b c) to defining functions instead of (define foo (lambda (a b c)))

-When declaring functions that return lambda procedures use (define (foo a b c)(lambda(x)))
   Eg: (define (foo a b c)
        (lambda (x)
         (+ a b c x)))

-If you use functions that receive n-arity values use an un-parenthesis lambda:
    Eg: (define foo
	  (lambda x
	    (if (null? x)
	        '()
	        (+ (car x) (foo (cdr x))))))

-Optional values should use dot notation:
    Eg: (define (foo . x))

-Do not declare global variables unless you REALLY want to have the vars accesible from all functions.
 
-Indent with two spaces every time you use cond, if,lambda, let, letrec,
begin and follow the traditional scheme indenting procs.
   Eg: (define (foo a b c)
         (let* ((a 1) (b 1))	
           (+ a b)) )
*if you're an emacs user indent and spacing are automatically done by the Scheme package.

-Use as far as possible functional aproach to solve problems. Don't use do, set!, read
 or whatever thing that might resemble imperative programming unless it is really
 necesary. Also try to work with lists instead of vectors, arrays or structures. 

-Whenever you use recursion use proper-tail-recursive approaches. 

-Do not declare procedures that depend on SLIB. 

-Do not declare functions using non-english terms. 

-Use define-macro to create templates for similar functions. Name the macro defs as
 define-whatever and the functions galway-function-whatever-it-does

-Whenever you are using global variables try to use two words name.
   E.g. (lang:about) (galway-publish) etc.

-Whenever you send me a patch comment your code with the way it works
 following the format shown on the code.

-Cleaning code in Galway is more important than trying not to break existing code.   

-Code clean ups are always welcome. 

-Do not submit work that 'tis just a temporary workaround for a full-fledged feature, i.e.
 do not submit "quick hacks" that are not designed to be expanded upon.
 It is better to submit an implementation that's been designed to be expanded & enhanced,
 although 'tis not finished.

-Do not use guile "system" function. This may lead to possible security hazards. 
 Everything should be coded in Scheme unless it is REALLY necesary to code something in C, C++
 or any other programming language.
 
- It is more important to be correct than to be fast.

-Whenever you open a Scheme port always close it after you have used it. 

-Do not work on an specific feature unless I've already aproved it for future inclusion in
 the programme.

-Do not make changes in the CVS sources unless I've already instructed you to do so.

Ariel Rios
July 1999

Last Revision:December 30 1999

*Document based on the GNUmeric hacking guide by Miguel de Icaza*






