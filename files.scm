;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; ariel@arcavia.com        ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; This program is part of the GNU project, released
;; under the aegis of GNU and released under the GPL

;; Functions and variable for multi file management.

;; (galway-read-string-file file)

;; (galway-document-new xml arg)
;; (galway-document-new-insert xml arg)
;; (galway-dialog-new-signal xml)
;; (galway-dialog-new xml arg)

;; (galway-document-open xml arg)
;; (galway-dialog-open-signal xml)
;; (galway-dialog-open  xml arg)

;; (galway-document-save xml arg)
;; (galway-dialog-save-as-signal xml)
;; (galway-dialog-save-as xml arg)

;; (galway-document-load-as-scheme xml arg)
;; (galway-dialog-load-as-scheme-signal xml
;; (galway-dialog-load-as-scheme xml arg)

;; (galway-document-close xml arg)

;; (galway-dialog-exit-signal xml)
;; (galway-dialog-exit xml arg)

;; (galway-document-clear-all xml arg)
;; (galway-document-change-current-document xml arg)

;; (galway-document-put-into-buffer xml)

;; (galway-dialog-file-signal xml)


;; The multiple file engine is designed in the following way.
;; galway-document-index is an integer that tells us the actual document
;; galway-document-file-name is a list containing the names of the opened files
;; galway-document-buffer is a list containing buffers of the available files.
;; galway-document-permission is a list containing ints indicating the file permissions

;; Read string file

(define (galway-read-string-file file)
  (let* ((fd (open file O_RDONLY)))
    (letrec ((read-string 
	      (lambda(str)
		(let ((rd (read-line fd)))
		  (if (eof-object? rd)
		      str
		      (read-string (string-append str rd "\n")))))))
      (read-string ""))))


;; New document

(define (galway-document-new xml arg)
  (let ((text (glade-xml-get-widget xml "text1")))

    ;; we copy the actual Gtktext content into the buffer
    (cond 
     ((not galway-document-index) (set! galway-document-index 0))
     (else
      (list-set! galway-document-buffer galway-document-index (gtk-editable-get-chars text 0 -1))))

    ;; we clear the current content and update the vars...
    
    (galway-document-clear-all xml (gtk-text-get-length text))   
     
    ;; we add a new item unto the clist...
      (gtk-clist-append (glade-xml-get-widget xml "clist2") (vector "UNTITLED"))

    (set! galway-document-file-name (galway-list-add galway-document-file-name "UNTITLED"))
    (set! galway-document-buffer (galway-list-add galway-document-buffer arg))
    ;(set! galway-document-permission (galway-list-add galway-document-buffer perms))

    (galway-document-new-insert xml arg)

    (set! galway-document-index (1- (length galway-document-buffer)))
    
  (gtk-widget-grab-focus text)))

(define galway-template-html "")
(define galway-template-vrml "")
(define galway-template-jscript "")
;; This function receives as an argument the selected icon index.

(define (galway-document-new-insert xml arg)
  ;;(include "templates.scm")
  (gtk-text-insert (glade-xml-get-widget xml "text1") #f
		   (case arg
		     ((0 2) "red")
		     ((1) "blue")
		     (else "black"))
		   #f
		   (case arg
		     ((0) galway-template-html)
		     ((1) galway-template-vrml)
		     ((2) galway-template-jscript)
		     (else ""))
		   -1))



;; We are going to connect the new document dialog. Each time we select
;; an icon we reset the galway-icon val to the value returned by the signal.

(define (galway-dialog-new-signal xml)
  
  (define galway-icon 0)

  (gnome-icon-list-append (glade-xml-get-widget xml "iconlist1")
			  (include-image "gnome-html.png")
			  "New HTML Document")
  (gnome-icon-list-append (glade-xml-get-widget xml "iconlist1")
			  (include-image "gnome-logo-icon-transparent.png")
			  "New VRML 2.0 Document")  
  (gnome-icon-list-append (glade-xml-get-widget xml "iconlist1")
			  (include-image "gnome-logo-icon-transparent.png")
			  "New Jscript Document")
  (gnome-icon-list-append (glade-xml-get-widget xml "iconlist1")
			  (include-image "gnome-note.png")
			  "New Blank Document")
  
  (gtk-signal-connect (glade-xml-get-widget xml "button102") "clicked"
		      (lambda ()
			(galway-document-new xml galway-icon)
			(gtk-button-clicked (glade-xml-get-widget xml "button104"))))
      
  (gtk-signal-connect (glade-xml-get-widget xml "button104") "clicked"
		      (lambda ()
			(gtk-widget-hide-all (glade-xml-get-widget xml "new1"))))

  (gtk-signal-connect (glade-xml-get-widget xml "iconlist1") "select-icon"
		      (lambda (num event)
			(set! galway-icon num))))

(define (galway-dialog-new xml arg)

  (gtk-widget-show-all (glade-xml-get-widget xml "new1")))

;; Each time we open a document the string we copy the current content unto the buffer
;; signaled by galway-document-index 
;; Then we read the file and put the string unto the text widget and unto a new element
;; galway-document-buffer. Finally we set the galway-document-index.

;; The following function is the low level engine for making it happen;
;; as always it receives two arguments. In this function, arg is null.
;; The arguments we are going to need are all taken from the xml
;; and from the global variables. We preserve arg for compatibilty
;; with the galway engine.

(define (galway-document-open xml arg)
  (let ((text (glade-xml-get-widget xml "text1")))
	
	;; first we get the content of the old string unto the buffer
        ;; if we we already had one...
    
    (cond 
     ((not galway-document-index) (set! galway-document-index 0))
     (else
      (list-set! galway-document-buffer galway-document-index (gtk-editable-get-chars text 0 -1))))

	;; We are now ready to open the new file:

    (let ((str (galway-read-string-file arg)))

      (cond 
       ((string? str)

	;; first we clear the text widget and we write the new string unto the text widget:
	    
	(galway-document-clear-all xml (gtk-text-get-length text))
	(gtk-text-insert text #f "black" #f str -1)

	;; now we are ready to update the global vars:
	;; first we add the file name unto the galway-document-file-name list,
	;; then we add str unto galway-document-buffer 
	;; and galway-document-permission:
	
	(set! galway-document-file-name (galway-list-add galway-document-file-name arg))
							
	(set! galway-document-buffer (galway-list-add galway-document-buffer arg))

	;(set! galway-document-permission (galway-list-add galway-document-buffer perms))
	   	    
	;; We update the index:

	(set! galway-document-index (1- (length galway-document-buffer)))
	
	;; Finally we put the file on the opened file tree...

	(gtk-clist-append (glade-xml-get-widget xml "clist2")
			  (vector (galway-file-from-path-to-file arg)))

       ;; We check if document is html to put or not on gtkhtml...
       
	(galway-parse-file xml arg)
	(gtk-widget-grab-focus text))
       (else 
	;; a warning message should go here...
	(display "Can't open"))))))
	  
;; The signals for the open dialog...

(define (galway-dialog-open-signal xml)
 
 (gtk-signal-connect (glade-xml-get-widget xml "ok_button1") "clicked"
		     (lambda()
		       (galway-document-open xml
					     (gtk-file-selection-get-filename 
					      (glade-xml-get-widget xml "open-html1")))
		       (gtk-button-clicked  (glade-xml-get-widget xml "cancel_button1"))))

 (gtk-signal-connect (glade-xml-get-widget xml "cancel_button1") "clicked"
		     (lambda () (gtk-widget-hide (glade-xml-get-widget xml "open-html1")))))
					      

;; This the actual function we are going to call.
;; arg is not used here.

(define (galway-dialog-open xml arg)
  (gtk-file-selection-set-filename (glade-xml-get-widget xml "open-html1") "")
  (gtk-widget-show-all (glade-xml-get-widget xml "open-html1")))
	
;; The following set of functions will help us to implement
;; save and save as functionality.

;; This function is the main low level cheese for
;; saving the documents. Both, save a nd save-as will use it
;; However, usage will be a little diffrent
;; For save we are going to call it by a:
;;    (galway-document-save xml #f)
;; Internally the function will call the name of the file from the name buffer list

;; For save as we are going to call it from the save-as dialog:
;;    (galway-document-save xml "name-of-file")
;; In this case we'll grab the name of the function from the given string.

(define (galway-document-save xml arg)
  (call-with-current-continuation
   (lambda (break)
     (cond
      ;;We check if we have opened a document:
      ((not galway-document-index) #f)
      ;; Have we already given a name for the document?
      ((equal? "UNTITLED" (if arg
			      (galway-file-from-path-to-file arg)
			      (list-ref galway-document-file-name galway-document-index)))
       (gtk-widget-show-all (glade-xml-get-widget (glade-xml-new xml-file  "messagebox2")
						  "messagebox2")) (break #f)) )
     
     
      (let* ((out* (open-output-file (if arg 
					 arg				   
				     (list-ref galway-document-file-name galway-document-index))))
	     (str (gtk-editable-get-chars (glade-xml-get-widget xml "text1") 0 -1)))     

	  (for-each (lambda (c) (write-char c out*)) (string->list str))

	  (if (not arg) (break #f))

	  (list-set! galway-document-file-name galway-document-index arg)

	  (let ((clist (glade-xml-get-widget xml "clist2")))
	    (gtk-clist-remove clist galway-document-index)
	    (gtk-clist-insert clist galway-document-index (vector (galway-file-from-path-to-file arg))))
	  
	  (close-output-port out*)
	  (break #t)))))

(define (galway-dialog-save-as-signal xml)
 
 (gtk-signal-connect (glade-xml-get-widget xml "ok_button3") "clicked"
		     (lambda()
		       (gtk-button-clicked  (glade-xml-get-widget xml "cancel_button3"))
		       (galway-document-save xml
					     (gtk-file-selection-get-filename 
					      (glade-xml-get-widget xml "save-as-dialog1")))))
		       
 (gtk-signal-connect (glade-xml-get-widget xml "cancel_button3") "clicked"
		     (lambda ()
		       (gtk-widget-hide (glade-xml-get-widget xml "save-as-dialog1")))))
  
(define (galway-dialog-save-as xml arg) 
  (gtk-file-selection-set-filename (glade-xml-get-widget xml "save-as-dialog1") "")
  (gtk-widget-show-all (glade-xml-get-widget xml "save-as-dialog1")))

;; In the future, galway will be able to do scripting via guile
;; so with the following set of functions we load unto the interpreter
;; a selected scheme file.

(define (galway-document-load-as-scheme xml arg) (load-from-path arg))

(define (galway-dialog-load-as-scheme-signal xml)
  
  (gtk-signal-connect (glade-xml-get-widget xml "ok_button2") "clicked"
		      (lambda ()
			(galway-document-load-as-scheme xml
					      (gtk-file-selection-get-filename
					       (glade-xml-get-widget xml "load-scheme1")))
			(gtk-button-clicked (glade-xml-get-widget xml "cancel_button2"))))
  
  (gtk-signal-connect (glade-xml-get-widget xml "cancel_button2") "clicked"
					    (lambda ()
					      (gtk-widget-hide-all 
					       (glade-xml-get-widget xml "load-scheme1")))))

(define (galway-dialog-load-as-scheme xml arg) (gtk-widget-show-all (glade-xml-get-widget xml "load-scheme1")))

;; Low level function for closing a file
;; We are going to remove the text from the widget
;; and all the current references to the document
;; on the various indexes:

(define (galway-document-close xml arg)

  (define str (gtk-editable-get-chars (glade-xml-get-widget xml "text1") 0 -1))
  
  (call-with-current-continuation

   (lambda (break)

     (cond
      ((not galway-document-index) (break #f))
      
      ((not (equal? str (list-ref galway-document-buffer galway-document-index))) (break #f)))

     ;; We pass the first part of the continuation now...

     (galway-document-clear-all xml (string-length str))

     (set! galway-document-buffer (delete 
				   (list-ref galway-document-buffer galway-document-index) 
				   galway-document-buffer))
     

     (set! galway-document-file-name (delete (list-ref galway-document-file-name galway-document-index) galway-document-file-name))

     ;(set! galway-document-permission (delete (list-ref galway-document-permission galway-document-index) galway-document-permission))

      (gtk-clist-remove (glade-xml-get-widget xml "clist2") galway-document-index)

     ;; finally we set the new index:

     (set! galway-document-index #f)
      
     (break #t))))

;; Exit functions:

(define (galway-dialog-exit-signal xml)
  
  (gtk-signal-connect (glade-xml-get-widget xml "button106") "clicked" (lambda () (gtk-main-quit)))

  (gtk-signal-connect (glade-xml-get-widget xml "button107") "clicked" (lambda () (gtk-widget-hide-all (glade-xml-get-widget xml "exit-dialog1")))))

(define (galway-dialog-exit xml arg)

  ;; This is terrible. Hope to find a better way to do it =(

  (define xml (glade-xml-new xml-file "exit-dialog1"))

  (galway-dialog-exit-signal xml)

  (gtk-widget-show-all (glade-xml-get-widget xml "exit-dialog1")))

;; Clear all function:

(define (galway-document-clear-all xml arg)

  (define text (glade-xml-get-widget xml "text1"))

  (gtk-text-set-point text arg)

  (gtk-text-backward-delete text arg))


;; With this funky function we copy the current content of the buffer unto
;; its buffer, clear whatever we had there, and finally update the index:

(define (galway-document-change-current-document xml arg)

  (define text (glade-xml-get-widget xml "text1"))

  (if galway-document-index     
      (list-set! galway-document-buffer galway-document-index
		 (gtk-editable-get-chars text 0 -1)))

  (galway-document-clear-all xml (gtk-text-get-length text))
  
  (gtk-text-insert text #f #f #f (list-ref galway-document-buffer arg) -1)

  (set! galway-document-index arg)

  (galway-parse-file xml (list-ref galway-document-file-name arg))

  (gtk-widget-grab-focus text))

;; put the content of the widget unto the buffer
;; this should be extended to include both the text widget
;; and the gtkhtml edit component

(define (galway-dialog-put-into-buffer xml arg)
  (define text (glade-xml-get-widget xml "text1"))

  (if galway-document-index     
      (list-set! galway-document-buffer galway-document-index
		 (gtk-editable-get-chars text 0 -1))))
  

;; This is the file we call for generating the signals...

(define-public (galway-dialog-file-signal xml)

  (galway-dialog-new-signal xml)
  (galway-dialog-open-signal xml)
  (galway-dialog-save-as-signal xml)
  (galway-dialog-load-as-scheme-signal xml))
  


































