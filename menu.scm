;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

(load "menu-bar.scm")

;; Called functions for adding items unto all of the menus.
;; This is actually a wrapper of the rest of the functions.
;; However we only need to call one function to have the whole
;; thing done.

;; (galway-define-menu-all xml)
;; (galway-define-html xml)
;; (galway-define-vrml xml)
;; (galway-define-jscript xml)
;; (galway-define-script-fu xml)

;; This function append the items unto the html menu

(define (galway-define-html xml)
  (galway-menu-creator xml 
		       (map glade-xml-get-widget 
			    (list xml xml) 
			    (list "html1_menu" "html2_menu"))
		       (list "Top level" "Head" "Block level"
			     "Special inline" "Other" "Font style"
			     "Frames" "Dialogs")
		       (list galway-menu-html-top-level
			     galway-menu-html-head
			     galway-menu-html-block-level
			     galway-menu-html-special-inline 
			     galway-menu-html-other
			     galway-menu-html-font-style
			     galway-menu-html-frames
			     galway-menu-html-various)))

;; This function appends the items unto the vrml menu

(define (galway-define-vrml xml)
  (galway-menu-creator xml 
		   (map glade-xml-get-widget 
			    (list xml xml) 
			    (list "vrml1_menu" "vrml2_menu"))
		       (list "Bindable" "Common" "Geometry"
			     "Group" "Interpolators"
			     "Properties" "Sensors"
			     "Other")
		       (list galway-menu-vrml-bindable
			     galway-menu-vrml-common
			     galway-menu-vrml-geometry
			     galway-menu-vrml-group
			     galway-menu-vrml-interpolator
			     galway-menu-vrml-properties 
			     galway-menu-vrml-sensors
			     galway-menu-vrml-other)))


;; This function append the items unto the jscript menu.

(define (galway-define-jscript xml)
  (galway-menu-creator xml
		       (map glade-xml-get-widget 
			    (list xml xml) 
			    (list "jscript1_menu" "jscript2_menu"))
		       (list "Statements" "Pre-built Functions")
		       (list galway-menu-jscript-statements
			     galway-menu-jscript-funcs)))
			
;; This function append the items unto the script-fu menu

(define (galway-define-script-fu xml)
  (galway-menu-creator xml
		   (map glade-xml-get-widget 
			    (list xml xml) 
			    (list "script-fu1_menu" "script-fu2_menu"))
		       (list "Script Fu Types" "Scheme")
		       (list galway-menu-script-fu-types
			     galway-menu-script-fu-scheme)))

;; This is the function we are going to call from galway-gnome.scm

(define (galway-define-menu-all xml)
  (galway-define-html xml) 
  (galway-define-vrml xml)
  (galway-define-jscript xml)
  (galway-define-script-fu xml))
  










