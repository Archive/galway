;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Functions and variables for adding items unto the Columned trees

;; galway-ctree-directory-files
;; galway-ctree-opened-files

;; (galway-ctree-read-dir arg)
;; (galway-ctree-dir-add-items xml arg)
;; (galway-ctree-dir-add-items-signal ixml)
;; (galway-ctree-open-files-signal xml)
;; (galway-ctree-dir-add-by-button-signal xml)
;; (galway-ctree-dir-signal xml)

;; The following global vars will keep track of the
;; content of both ctrees

(define-public galway-ctree-directory-files '())
(define-public galway-ctree-opened-files '())


;; This will be a little tricky to implement
;; first of all we need a function to open 
;; a directory and return a list of strings
;; with the content of the directory.
;; No checking is done. We do the assumption
;; that arg is an existing directory

(define (galway-ctree-read-dir arg)
  (let ((dir (opendir arg)))
    (letrec ((director
	      (lambda(ls)
		(let ((obj (readdir dir)))
		  (cond
		   ((eof-object? obj) ls)
		   (else (director (galway-list-add ls obj))))))))
      (director '()))))

;; We need a function that can add the items unto
;; the dir ctree. THis function should check that
;; the given argument is a directory

(define (galway-ctree-dir-add-items xml arg) 
 
  (call-with-current-continuation
   (lambda (break)
     (if (not (access? arg R_OK)) 
	 (break #f))
     ;; FIXME, this should check the dir

     ;;(if (file-is-directory? arg)
     ;; (break #f))
   
     ;; if we arrive here then we know that 
     ;; everything its ok so we can safely grab the values of the
     ;; computation

     (let ((clist (glade-xml-get-widget xml "clist1")))
       
       (gtk-clist-set-column-title clist 0 arg)
       (gtk-clist-clear clist)

       (set! galway-ctree-directory-files 
	     (map (lambda (val) 
		    (string-append arg val))
		  (galway-ctree-read-dir arg)))

       (map (lambda (v) (gtk-clist-append clist (vector v))) galway-ctree-directory-files) )
     
   (break #t))))

(define (galway-ctree-dir-add-items-signal xml)
 
 (gtk-signal-connect (glade-xml-get-widget xml "clist1") "select_row"
		     (lambda (row col event)	
		       ;; FIXME
			(if (file-is-directory? (list-ref galway-ctree-directory-files row))
			    (galway-ctree-dir-add-items xml (list-ref galway-ctree-directory-files row))
			    (galway-document-open xml (string-append "./" 
								     (list-ref 
								      galway-ctree-directory-files row)))))))
		      


;; This one will connect the appended strings in the opened files dir.

(define (galway-ctree-open-files-signal xml)
  (gtk-signal-connect (glade-xml-get-widget xml "clist2") "select_row"
		      (lambda (row col event) (galway-document-change-current-document xml row))))

;; THis function enables changing the current open directory

(define (galway-ctree-dir-add-by-button-signal xml)
  (gtk-signal-connect (glade-xml-get-widget xml "button7") "clicked"
		      (lambda() (galway-ctree-dir-add-items xml (gtk-entry-get-text (glade-xml-get-widget xml "entry2"))))))


;; The function we are going to call for connecting all the signals...

(define (galway-ctree-dir-signal xml)
  (galway-ctree-dir-add-by-button-signal xml)
  (galway-ctree-dir-add-items-signal xml)
  (galway-ctree-open-files-signal xml))









