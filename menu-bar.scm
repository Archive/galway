;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Menu insertion/creation functions:

;; (galway-menu-creator xml menu ls actions)

;; (galway-menu-item-signal xml item actions)
;; (galway-menu-item-all signal xml)

;; We are going to receive the xml, menu,
;; A list of names, list of actions.

;; So we'll do:
;; 1)Create menu items from names
;; 2)Create menu subitems from macro actions


;; We are no longer going to load all the functions from galway-gnome.scm
;; Instead we'll only load them in the library that require them

(load "./macro-menu.scm")

;; The main function
;; This one will create all the necesary new functions
;; Still we need some big changes on the macro for menu 
;; From now on we'll only pass the xml thingy. Internally,
;; we are goin to call the desired widgets. 

;; We are going to use this function to add items and signals
;; upon the menubar and upon the popup menu.

(define (galway-menu-creator xml menu ls actions)
  
  (letrec ((menu-creator 
	    (lambda (menu ls actions)
	      (if (null? ls)
		  #t
		  (let ((item (gtk-menu-item-new-with-label (car ls)))
			(menu-item (gtk-menu-new)))
		    
		    (gtk-menu-append menu item)
		    (gtk-menu-item-set-submenu item menu-item)
		    ((car actions) xml menu-item)
		    (menu-creator menu (cdr ls) (cdr actions))))))

	   (creator
	    (lambda (menu)
	      (cond
	       ((null? menu) #t)
	       (else
		(menu-creator (car menu) ls actions)
		(gtk-widget-show-all (car menu))
		(creator (cdr menu)))))))

    (creator menu)))

;; The following function will help us
;; to add signals to item created on glade



;; The function we are going to be calling =)

(define-public (galway-menu-item-all-signal xml)
  (for-each (lambda (item action) (gtk-signal-connect (glade-xml-get-widget xml item) "activate" (lambda ()(action xml #f))))
	    '("new_file1" "open1" "save1" "save_as1"  "load_scheme_file1" ;;"ftp_publishing1"  "ftp_client1"   
	      "exit1")
	    (list galway-dialog-new galway-dialog-open galway-document-save galway-dialog-save-as galway-dialog-load-as-scheme 
		 ;; galway-ftp-publish  galway-ftp-client 
		  galway-dialog-exit)))
			  

