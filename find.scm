;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Functions and variables for adding items unto the Columned trees

;; (galway-find-substring pattern-string)
;; (galway-document-find xml arg)
;; (galway-get-word-end str)

;; (galway-document-find-replace xml arg)
;; (galway-dialog-find-signal xml)
;; (galway-dialog-find xml arg)

;; (galway-dialog-find-replace-signal xml)
;; (galway-dialog-find-replace xml arg)

;; (galway-dialog-find-all-signal xml)

;; Original Author: Ken Dickey
;; Date 1991 August 6
;; Modified by Ariel Rios

;; the function (galway-find-substring)
;; takes a string (the "pattern") and returns a function
;; which takes a string (the "target") and either returns #f or the index in
;; the target in which the pattern first occurs as a substring.
;;
;; E.g.: ((galway-find-substring "test") "This is a test string")  -> 10
;;       ((galway-find-substring "test") "This is a text string")  -> #f

(define (galway-find-substring pattern-string)
  
  (define num-chars-in-charset 256)  

  (define (build-shift-vector pattern-string)
    
    (let* ((pat-len (string-length pattern-string))
  	   (shift-vec (make-vector num-chars-in-charset (+ pat-len 1)))
	   (max-pat-index (- pat-len 1)))
     
      (let loop ((index 0))

        (vector-set! shift-vec 
		     (char->integer (string-ref pattern-string index))
		     (- pat-len index))

	(if (< index max-pat-index)
	    (loop (+ index 1))
	    shift-vec) )))


  (let ((shift-vec (build-shift-vector pattern-string))
	(pat-len   (string-length pattern-string)))

    (lambda (target-string)
      
      (let* ((tar-len (string-length target-string))
	     (max-tar-index (- tar-len 1))
	     (max-pat-index (- pat-len 1)))
	
	(let outer ((start-index 0))
	  
	  (if (> (+ pat-len start-index) tar-len)
	      #f
	      (let inner ((p-ind 0) (t-ind start-index))
		(cond

		 ((> p-ind max-pat-index) #f)
		   
		 ((char=? (string-ref pattern-string p-ind) (string-ref target-string  t-ind))
		  (if (= p-ind max-pat-index)
		      start-index  ;; success -- return start index of match
		      (inner (+ p-ind 1) (+ t-ind 1)))) ; keep checking
                   
                   ((> (+ pat-len start-index) max-tar-index) #f) ; fail

		   (else
		     (outer (+ start-index
			       (vector-ref shift-vec
					   (char->integer 
				 		(string-ref target-string
							    (+ start-index pat-len) ))))))))))))))

;; Wow an interesting one!
;; The following function will be the one in charged of 
;; checking from where are we goin to start checking
;; In this function, arg is a list.
;; The car is the name of the string to be searched.
;; The cadr of the pair will tell us whether to tell us whether search is case-sensitive
;; The caddr will tell us whether to start from current point.

(define (galway-document-find xml arg)            
  
  (let* ((text (glade-xml-get-widget xml "text1")) 
	 (n ((galway-find-substring (car arg))
	     (gtk-editable-get-chars text (if (caddr arg) 0 (gtk-editable-get-position text)) -1))))
    
  
    (if n 
	(gtk-editable-select-region text n 
				    (+ n (galway-get-word-end (gtk-editable-get-chars text n -1))))

	(display "Can't find string"))))

;; The following function will return the index of the end of a word.

(define (galway-get-word-end str)
  
  (letrec ((word
	   (lambda (ls n)
	     (if (null? ls)
		 n
		 (case (car ls)
		   ((#\space #\newline #\tab #\. #\, #\:) n)
		   (else (word (cdr ls) (1+ n))))))))

    (word  (string->list str) 0)))

(define (galway-dialog-find-signal xml)
 
  (define-public galway-find-case-sensitive #t)
  (define-public galway-find-begin #f)
  
  (gtk-signal-connect (glade-xml-get-widget xml "button89") "clicked"
		      (lambda ()
			(gtk-button-clicked (glade-xml-get-widget xml "button90"))
			(gtk-button-clicked (glade-xml-get-widget xml "button91"))))

  (gtk-signal-connect (glade-xml-get-widget xml "button90") "clicked"
		      (lambda ()
			(galway-document-find xml (list (gtk-entry-get-text 
						     (glade-xml-get-widget xml "combo-entry13"))
						    galway-find-case-sensitive
						    galway-find-begin))))

  (gtk-signal-connect (glade-xml-get-widget xml "button91") "clicked"
		      (lambda ()
			(gtk-widget-hide-all (glade-xml-get-widget xml "find1"))))

  (gtk-signal-connect (glade-xml-get-widget xml "checkbutton11") "toggled"
		      (lambda ()
			(set! galway-find-case-sensitive (not galway-find-case-sensitive))))

  (gtk-signal-connect (glade-xml-get-widget xml "checkbutton12") "toggled"
		      (lambda ()
			(set! galway-find-begin (not galway-find-begin)))))

(define (galway-dialog-find xml arg)
  (gtk-entry-set-text (glade-xml-get-widget xml "combo-entry13" ""))
  (gtk-widget-show-all (glade-xml-get-widget xml "find1")))   

;; The find-replace function are very similar than the latter functions
;; The dif'rence is that once we've find the string we replace it with another...
;; The car of the arg is the string to be find
;; The cadr is the string to replace  the car with
;; The caddr is the flag for case sensitive
;; The cadddr is the flag for start at the beginning...

;; If we find the car on position n
;; then we clear the text box, write the old content until n -1,
;; write then the cadr on position n, then restart writing
;; the old content on n + strlen(car).

(define (galway-document-find-replace xml arg)
  
    (let* ((text (glade-xml-get-widget xml "text1")) 
	   (n ((galway-find-substring (car arg))
	       (gtk-editable-get-chars text (if (cadddr arg) 0 (gtk-editable-get-position text)) -1))))
    
  
    (if n 

	(let* ((str (gtk-editable-get-chars text 0 -1))
	       (head (substring str 0 n))
	       (tail (substring str (+ n (string-length (car arg))) (string-length str)))
	       (new  (string-append head (cadr arg) tail)))

	  (galway-document-clear-all xml (string-length str))
	  (gtk-text-insert text #f #f #f new -1)
	  (gtk-widget-grab-focus text))

	(display "String not found"))))

(define (galway-dialog-find-replace-signal xml)
 
  (define-public galway-find-replace-case-sensitive #t)
  (define-public galway-find-replace-begin #f)
  
  (gtk-signal-connect (glade-xml-get-widget xml "button94") "clicked"
		      (lambda ()
			(gtk-button-clicked (glade-xml-get-widget xml "button95"))
			(gtk-button-clicked (glade-xml-get-widget xml "button96"))))

  (gtk-signal-connect (glade-xml-get-widget xml "button95") "clicked"
		      (lambda ()
			(galway-document-find-replace xml (list (gtk-entry-get-text 
							 (glade-xml-get-widget xml "combo-entry14"))
							(gtk-entry-get-text
							 (glade-xml-get-widget xml "combo-entry15"))
							galway-find-case-sensitive
							galway-find-begin))))

  (gtk-signal-connect (glade-xml-get-widget xml "button96") "clicked"
		      (lambda ()
			(gtk-widget-hide-all (glade-xml-get-widget xml "find-replace1"))))

  (gtk-signal-connect (glade-xml-get-widget xml "checkbutton14") "toggled"
		      (lambda ()
			(set! galway-find-replace-case-sensitive (not galway-find-case-sensitive))))

  (gtk-signal-connect (glade-xml-get-widget xml "checkbutton15") "toggled"
		      (lambda ()
			(set! galway-find-replace-begin (not galway-find-begin)))))


(define (galway-dialog-find-replace xml arg)
  (gtk-entry-set-text (glade-xml-get-widget xml "combo-entry14" ""))
  (gtk-entry-set-text (glade-xml-get-widget xml "combo-entry15" ""))
  (gtk-widget-show-all (glade-xml-get-widget xml "find-replace1"))) 

(define (galway-dialog-find-all-signal xml)

  (galway-dialog-find-signal xml)
  (galway-dialog-find-replace-signal xml))






