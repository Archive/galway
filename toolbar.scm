;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; This file contain functions to connect
;; the toolbar buttons with desired signals

;; (galway-toolbar-button-signal-connect xml ls action)
;; (galway-toolbar-button-signal xml)

;; This function receives the xml and three lists containing
;; the name of the button, the action to be done, and the argument
;; to be passed. 

(define (galway-toolbar-button-signal-connect xml ls action arg)
 
  (letrec ((connect 
	    (lambda (ls action arg)
	      (cond
	       ((null? ls) #t)
	       (else 
		(gtk-signal-connect (glade-xml-get-widget xml (car ls)) "clicked"
				    (lambda ()
				      ((car action) xml (car arg))))
		(connect (cdr ls) (cdr action) (cdr arg)))))))
   
    (connect ls action arg)))
				      
;; This is the function that we are going to call...

(define-public (galway-toolbar-button-signal xml)

  ;; For clarity, we are goin to call the above function
  ;; as many times as toolbars we may have. 
  ;; I know that everything could go in a simple call
  ;; but I prefer mantainable code.

   ;; First toolbar (FILE):

  (galway-toolbar-button-signal-connect xml
					(list "button1"  "button2" "button3"
					      "button36" "button37") 
;					      "button40" "button41" "button42"
;					      "button43" "button44" "button45"
;                                              "button46" "button47")
					(list galway-dialog-new galway-dialog-open galway-document-save
					      galway-dialog-save-as galway-document-close) 
					      ;galway-dialog-print
;					      galway-text-cut galway-text-copy galway-text-paste
;					      galway-dialog-search galway-dialog-search-replace 
;					      galway-dialog-properties galway-dialog-preferences
;					      galway-dialog-about)
					(list #f #f #f #f #f))


  ;; Second toolbar (HTML):

  (galway-toolbar-button-signal-connect xml
					(list "button8"   "button9"  "button10"
					      "button11"  "button12" "button13"
					      "button14"  "button15" "button16"
					      "button18"  "button19" "button20"
					      "button21"  "button22")
					      ;"button23" "button24"
					      ;"button25"  "button26")
					(list  galway-tags galway-tags galway-tags 
					       galway-tags galway-tags galway-tags 
					       galway-tags galway-tags galway-tags
					       galway-dialog-html-list
					       galway-dialog-html-list
					       galway-dialog-html-table
					       galway-dialog-html-image
					       galway-dialog-html-link)
					(list "B"  "I"  "U"
					      "H1" "H2" "H3"
					      "LEFT" "CENTER" "RIGHT"
					      #f #t #f #f #f ) ) )



