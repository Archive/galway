;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; ariel@arcavia.com        ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Functions and variable for file extension management.


;; (galway-file-ext? xml str)

;; (galway-file-htm str)
;; (galway-file-html str)
;; (galway-file-perl? str)
;; (galway-file-php str)
;; (galway-file-php3t str)
;; (galway-file-scheme? str)
;; (galway-file-vrml? str)

;; (galway-file-html? str)
;; (galway-file-php? str)

;; (galway-parse-html xml str)
;; (galway-parse-perl xml str)
;; (galway-parse-php xml str)
;; (galway-parse-scheme xml str)
;; (galway-parse-vrml xml str)

;; (galway-parse-file xml str)

(define-macro (galway-file-ext? name extension)
  `(define (,name str)
     (equal? (string->list ,extension) 
	     (string->list (substring str 
				      (- (string-length str) 
					 (string-length ,extension))
				      (string-length str)))) ))

(galway-file-ext? galway-file-htm  ".htm")
(galway-file-ext? galway-file-html ".html")
(galway-file-ext? galway-file-perl? ".perl")
(galway-file-ext? galway-file-php  ".php")
(galway-file-ext? galway-file-php3t ".php3t")
(galway-file-ext? galway-file-scheme? ".scm")
(galway-file-ext? galway-file-vrml? ".vrml")

(define (galway-file-html? str) (or (galway-file-html str) (galway-file-htm str)))

(define (galway-file-php? str)(or (galway-file-php str) (galway-file-php3t str)))

;; parsing functions.

(define (galway-parse-html xml arg)
  
  (gtk-widget-realize html)
  (let ((buffer (if (string? arg) (galway-read-string-file arg) (list-ref galway-document-buffer arg))))
    (gtk-html-load-empty html)
    (gtk-html-load-from-string html buffer (string-length buffer))))
    
 
(define (galway-parse-php xml str)

  (system (string-append "php -q " str " > ./output.html"))
  (galway-parse-html "./output.html")
  (delete-file "./output.html"))

(define (galway-parse-file xml str)
  (cond 
   ((number? str) (galway-parse-file xml (list-ref galway-document-file-name str)))
   ((galway-file-html? str) (galway-parse-html xml str))
   ;((galway-file-perl? str) (galway-parse-perl str))
   ((galway-file-php?  str) (galway-parse-php  str))
   ;((galway-file-scheme? str) (galway-parse-scheme str))
   ;((galway-file-vrml? str) (galway-parse-vrml str)))))
))















