;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; ariel@arcavia.com        ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Function for galway scripting

;; (galway-scripting-signal xml)

(define-public (galway-scripting-signal xml)
  (gtk-signal-connect (glade-xml-get-widget xml "button6") "clicked"
		      (lambda ()   
			(eval-string (gtk-entry-get-text (glade-xml-get-widget xml "entry1")))
			(gtk-entry-set-text (glade-xml-get-widget xml "entry1") ""))))



