;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Functions for the plugin manager.

;; galway-plugin-list
;; galway-plugin-list-av

;; (galway-plugin-available xml arg)
;; (galway-plugin-available-signal xml arg)
;; (galway-plugin-load xml arg)

;; (galway-plugin-dialog-signal xml arg)

(define galway-plugin-list '())
(define galway-plugin-list-av '())


;; The following function reads if there is a .galway directory
;; if so, it loads every file.
;; We understand that all the files located in such a directory 
;; need to be scheme files

(define (galway-plugin-available xml arg)

  (call-with-current-continuation
   (lambda (break)
     
     (let ((plugin-dir (string-append (getenv "HOME") "/.galway")))

     (if (not (and (access? plugin-dir R_OK) (file-is-directory? plugin-dir)))
	 (break #f))

     ;; Ok... now we proceed to get the list of available files...

     (letrec ((director
	       (lambda (dir ls)
		 (let ((obj (readdir dir)))

		   (if (eof-object? obj)
		       ls
		       (director dir (galway-list-add ls obj)))))))

       (let* ((dir (opendir plugin-dir))
	      (dir-files (director dir '()))
	      (clist (glade-xml-get-widget xml "clist5")))

	 (set! galway-plugin-list-av dir-files)

	 (map (lambda (v) (gtk-clist-append clist (vector v))) dir-files))) )

     (break #t))))


(define (galway-plugin-available-signal xml)
  
  (gtk-signal-connect (glade-xml-get-widget xml "clist5") "select_row"
		      (lambda (row col event)
			(gtk-clist-append (glade-xml-get-widget xml "clist6") (vector 
									       (list-ref galway-plugin-list-av row)))

			(set! galway-plugin-list (galway-list-add galway-plugin-list 
								  (list-ref galway-plugin-list-av row))))) )
  
       
;; This will keep the files to load
;; next time galway is started

(define (galway-plugin-load xml arg)

  (map (lambda (v) (gtk-clist-append (glade-xml-get-widget xml "clist6") (vector v))) arg)
  (map (lambda (f) (load-from-path (string-append (getenv "HOME") "/.galway/" f))) arg))

 (define (galway-plugin-dialog-signal xml)

  (galway-plugin-available-signal xml))





