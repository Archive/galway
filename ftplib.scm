;;; ftplib for guile/scheme
;;; license LGPL. . .see www.gnu.org
;;;        copyright Brad Knotwell 1999

;;; usage:         see the end of the file for examples

;;; bugs:          I'd imagine so :-)

;;; todo 
;;;
;;; 1) proxy ftp
;;; 2) a more elegant select hack
;;; 3) code cleanup
;;; 4) a better copy-port for windows/unix issues
;;; 5) more testing :-)

;;; global variable section
(define ftplib:FTP-PORT 21)
(define ftplib:select-timeout 1000000)


(define ftplib:error-reply 'ftplib.error-reply)
(define ftplib:error-port 'ftplib.error-port)
(define ftplib:error-temp 'ftplib.error-temp)
(define ftplib:error-perm 'ftplib.error-perm)
(define ftplib:error-proto 'ftplib.error-proto)
(define ftplib:ftp-init-error 'ftplib.init-error) 
(define ftplib:dns-error 'ftplib.dns-error) 
(define ftplib:eof-error 'ftplib.eof-error) 
(define ftplib:invalid-arg 'ftplib.invalid-arg) 

(define ftplib:pf-pair (map make-regexp (list "^[23][0-9][0-9] " "^[45][0-9][0-9] "))) 
(define ftplib:transfer-mode 'port)

;;; private function section
;;; function takes a string and splits by character into a list of lists
;;; NOTE:  the order is reversed
(define (ftplib:split-string-by-char in-string split-char)
  (define (inner lst accum)
    (cond ((null? lst) (list accum))
	  ((eq? (car lst) split-char)
	   (if (length accum) 
	       (append (inner (cdr lst) '()) (cons accum '()))
	       (inner (cdr lst) '())))
	  (else (inner (cdr lst) (append accum (list (car lst)))))))
  (map list->string (inner (string->list in-string) '())))

;;; similar to pick function in common-list.scm
;;; however, it doesn't return a reversed list
(define (ftplib:my-pick p lst)
  (cond ((null? lst) '())
	 ((p (car lst)) (cons (car lst) (ftplib:my-pick p (cdr lst))))
	 (else (ftplib:my-pick p (cdr lst)))))

;;; remove char from a string
(define (ftplib:remove-char in-string chr)
  (list->string (ftplib:my-pick (lambda (x) (not (eq? x chr))) (string->list in-string))))

;;; remove carriage-returns and linefeeds
(define (ftplib:remove-CR-LF in-string)
  (ftplib:remove-char (ftplib:remove-char in-string #\cr) #\newline))

;;; copy an input port to an output port
;;; side effects:  closes both input and output ports

(defmacro map-dot-pair (fun pair)
  `(list (,fun (car ,pair)) (,fun (cdr ,pair))))

(define (ftplib:copy-port input-port output-port)
  (let ((data (read-line input-port 'split)))
    (if (eof-object? (cdr data))
	(begin
	  (if (not (eof-object? (car data)))
	      (display (car data) output-port))
	  (car (map close (list input-port output-port))))
	(begin
	  (map-dot-pair (lambda (x) (display x output-port)) data)
	  (ftplib:copy-port input-port output-port)))))
	

;;; given a control port issue a command
;;; returns (#t . matched-line) or (#f . matched-line)
;;;   NOTE:  in the event of an exception (ie timeout) the matched-line
;;;          will be null
;;;   NOTEII: given a NULL command -> don't send a command (duh)
;;;   NOTEIII:  there's a minor (major?) hack in here.  In general, 
;;;             ftp does _not_ acknowledge a successful data request
;;;             immediately.  It instead waits until it's ready to send.
;;;             In passive mode this is not so bad because you can connect
;;;             before sending the data command.  In port mode, this is
;;;             a problem because the server doesn't connect until after
;;;             the 
(define (ftplib:ftp-cmd control-port command pass-fail-regexp . file-op-thunk)
  (define (read-til)
    (let ((data (read-line control-port)))
      (cond ((regexp-exec (car pass-fail-regexp) data) (cons #t (ftplib:remove-CR-LF data)))
	    ((regexp-exec (cadr pass-fail-regexp) data) (cons #f (ftplib:remove-CR-LF data)))
	    (else (read-til)))))
    (catch #t
	   (lambda ()
	     (let ((empty (if (not (string=? command "")) (write-line command control-port) ""))
		   (file-op (if (null? file-op-thunk) #f (car file-op-thunk))))
	       (if (not file-op)
		   (read-til)
		   (begin
		     (let ((read-fd-list (car (select (list control-port) '() '() 0 ftplib:select-timeout))))
		       (cond ((null? read-fd-list) (begin (file-op) (read-til)))
			     (else 
			      (let ((data-read (read-line control-port)))
				(if (regexp-exec (make-regexp "^1[0-9][0-9]") data-read)
				    (begin (file-op) (read-til))
				    (cons #f data-read))))))))))
	   (lambda args args)))

;;; directory op
(define (ftplib:dir-op control-port . optargs)
  (let ((command (cond 
		  ((eq? (length optargs) 1) (car optargs))
		  (else (string-append (car optargs) " " (cadr optargs))))))
  (ftplib:ftp-cmd control-port command ftplib:pf-pair)))

;;; generic data operations returns a pair (#t . last-response) on
;;; success (#f . last-response on error 
;;; NOTE: handler function is a function taking an open port (either for input 
;;; or output as an argument (this depends on the command being issued 
(define (ftplib:data-op control-port cmd-string handler-func . mode)
  (let* ((mode-setting (if (null? mode) ftplib:transfer-mode (car mode)))
	 (data-conn (cdr (ftplib:get-data-socket control-port mode-setting))))
      (ftplib:ftp-cmd control-port cmd-string ftplib:pf-pair (lambda () (handler-func mode-setting data-conn)))))

;;; get the data socket
;;; returns a status . socket pair
;;;       (#f . '()) --> failed establishing data socket
;;;       (#t . socket)
;;;
;;;     NOTE:  this is really nasty, it wasn't really clear how to
;;;            make it nicer
(define (ftplib:get-data-socket control-port . mode)
  (define (create-socket)
    (let ((my-sock (socket AF_INET SOCK_STREAM 0)))
      (begin 
	(setsockopt my-sock SOL_SOCKET SO_REUSEADDR 1)
	(setsockopt my-sock SOL_SOCKET SO_LINGER (cons 0 0))
	my-sock)))
  (define (parse-passive-addr addr)
    (define (get-addr addr-list mask-list)
      (let ((mask (apply * mask-list)))
	(if (null? mask-list)
	    (string->number (car addr-list))
	    (+ (* mask (string->number (car addr-list))) (get-addr (cdr addr-list) (cdr mask-list))))))
    (let* ((data (string->list addr))
	   (flt (ftplib:my-pick (lambda (x) (not (or (eq? x #\() (eq? x #\))))) data))
	   (addr-list (ftplib:split-string-by-char (list->string flt) #\,))
	   (list-grab (lambda (l) (map (lambda (z) (list-ref addr-list z))) l))
	   (port (list-grab '(4 5)))
	   (inetaddr (list-grab '(0 1 2 3))))
      (cons (get-addr inetaddr '(256 256 256))
	    (get-addr port '(256)))))
  (define (parse-port-addr portnum)
    (define (addr-list addr mask-list)
      (let* ((mask (apply * mask-list))
	     (octet (quotient addr mask)))
	(if (eq? mask 1)
	  (cons octet '())
	  (cons octet (addr-list (- addr (* mask octet)) (cdr mask-list))))))
    (let ((hostaddr (array-ref (getsockname control-port) 1)))
      (append (addr-list hostaddr '(256 256 256)) (addr-list portnum '(256)))))
  (define (format-addr-list addr-list)
    (let ((my-string (apply string-append (map (lambda (x) (string-append (number->string x) ",")) addr-list))))
        (substring my-string 0 (- (string-length my-string) 1))))
  (define (get-passive-sock in-sock)
    (let* ((cmd-resp (ftplib:ftp-cmd control-port "PASV" ftplib:pf-pair))
	   (parsed-cmd (ftplib:split-string-by-char (cdr cmd-resp) #\space))
	   (addr (parse-passive-addr (car parsed-cmd))))
	(if (not (car cmd-resp))
	    (cons #f '())
	    (begin
	      (connect in-sock AF_INET (car addr) (cdr addr))
	      (cons #t in-sock)))))
  (define (get-port-sock in-sock)
    (bind in-sock AF_INET INADDR_ANY 0)
    (listen in-sock 1)
    (let ((cmd-resp (ftplib:ftp-cmd control-port (string-append "PORT " (format-addr-list (parse-port-addr (array-ref (getsockname in-sock) 2)))) ftplib:pf-pair)))
      (if (not (car cmd-resp))
	  (cons #f '())
	  (cons #t in-sock))))
  (cond ((or (eq? mode 'port) (eq? ftplib:transfer-mode 'port))
	 (get-port-sock (create-socket)))
	((or (eq? mode 'passive) (eq? ftplib:transfer-mode 'passive))
	 (get-passive-sock (create-socket)))
	(else (error ftplib:invalid-arg "incorrect transfer type"))))

(defmacro port-check? (check-fun port action port-type)
     `(if (,check-fun ,port)
	  (,action)
	  (error ftplib:error-port (string-append "argument must be an " (symbol->string ,port-type)))))

(define (ftplib:determine-control-port xfer-type control-port)  
  (if (eq? xfer-type 'port) (car (accept control-port)) control-port))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;end private functions;;;;;;;;;;;;;;;;;;;;;;;;;

;;; the big cheese
;;; this function takes a hostname, username and passwd 
;;; NOTE:  username and passwd are optional with  
;;;        standard anonymous defaults
;;; returns a control port number
(define (ftplib:ftp host . user-passwd)
  (define user (if (eq? (length user-passwd) 2) (car user-passwd) "anonymous"))
  (define passwd (if (eq? (length user-passwd) 2) (cadr user-passwd) (string-append (array-ref (getpwuid (geteuid)) 0) "@" (array-ref (uname) 1))))
  (define my-port (socket AF_INET SOCK_STREAM 0))
  (define my-host (car (array-ref (gethostbyname host) 4)))
  (connect my-port AF_INET my-host ftplib:FTP-PORT)
  (let* ((user-ok? (ftplib:ftp-cmd my-port (string-append "USER " user) (list (make-regexp "^331 ") (cadr ftplib:pf-pair))))
	 (login-ok? (if (not user-ok?) #f (car (ftplib:ftp-cmd my-port (string-append "PASS " passwd) ftplib:pf-pair)))))
    (if login-ok? my-port '())))

;;; set the appropriate file type 
;;; appropriate values for xfer-type are 'image and 'ascii
;;; return (#t . response) on success (#f . response) on error
(define (ftplib:set-xfer-type control-port xfer-type)
  (cond ((eq? xfer-type 'image)  (ftplib:ftp-cmd control-port "TYPE I" ftplib:pf-pair))
	((eq? xfer-type 'ascii)  (ftplib:ftp-cmd control-port "TYPE A" ftplib:pf-pair))
	(else (error ftplib:error-proto "incorrect xfer-type"))))

;;; set the transfer-mode global variable
;;; NOTE: the transfer-mode defaults to global
(define (ftplib:set-transfer-mode mode) 
  (cond ((eq? mode 'port) (set! ftplib:transfer-mode mode))
	((eq? mode 'passive) (set! ftplib:transfer-mode 'passive))
	(else (error ftplib:invalid-arg "invalid argument: must be 'port or 'passive"))))

;;; attempt to get the remote system type
;;; returns a pair (#t . system-name) on success and (#f . "") on error
(define (ftplib:get-system-type control-port)
  (let ((cmd-resp (ftplib:ftp-cmd control-port "SYST" ftplib:pf-pair)))
    (if (not (car cmd-resp))
	(cons #f "")
	(cons #t (cadr (reverse (ftplib:split-string-by-char (cdr cmd-resp) #\space)))))))

;;; get the size in bytes of a particular file
;;; returns a pair (#t . num-bytes) on success and (#f . 0) on error
(define (ftplib:get-file-size control-port filename)
  (let ((cmd-resp (ftplib:ftp-cmd control-port (string-append "SIZE " filename) ftplib:pf-pair)))
    (if (not (car cmd-resp))
	(cons #f 0)
	(cons #t (string->number (car (ftplib:split-string-by-char (cdr cmd-resp) #\space)))))))

;;; get the last modification date to a file
;;; returns a pair (#t . date-string) on success and (#f . "") on error
(define (ftplib:get-file-modification-time control-port filename)
  (let ((cmd-resp (ftplib:ftp-cmd control-port (string-append "MDTM " filename) ftplib:pf-pair)))
    (if (not (car cmd-resp))
	(cons #f "")
	(cons #t (car (ftplib:split-string-by-char (cdr cmd-resp) #\space))))))

;;; make a directory returns a pair (#t . last-response) on success
;;; and (#f . last-response) on error 
(define (ftplib:make-directory control-port directory-name)
  (ftplib:dir-op control-port "MKD" directory-name))

;;; change directory returns a pair (#t . last-response) on success
;;; and (#f . last-response) on error 
(define (ftplib:change-directory control-port directory-name)
  (ftplib:dir-op control-port "CWD" directory-name))

;;; change to parent directory returns a pair (#t . last-response) on
;;; success and (#f . last-response) on error 
(define (ftplib:parent-directory control-port)
  (ftplib:dir-op control-port "CDUP"))

;;; delete the directory returns a pair (#t . last-response) on
;;; success and (#f . last-response) on error 
(define (ftplib:delete-directory control-port directory-name)
  (ftplib:dir-op control-port "RMD" directory-name))

;;; return current directory
;;; returns a pair (#t . directory) on success and (#f . "")
;;; on error
(define (ftplib:current-directory control-port)
  (let* ((resp (ftplib:dir-op control-port "PWD"))
	 (data (cdr resp)))
    (if (car resp)
	(cons #t (ftplib:remove-char (cadr (reverse (ftplib:split-string-by-char data #\space))) #\"))
	(cons #f ""))))

;;; rename the remote file
;;; returns a pair (#t . last-response) on success (#f . last-response)
;;; on error
(define (ftplib:rename-file control-port cur-name new-name)
  (let ((cmd-resp (ftplib:ftp-cmd control-port (string-append "RNFR " cur-name) ftplib:pf-pair)))
    (if (not (car cmd-resp))
	cmd-resp
	(let ((final-resp (ftplib:ftp-cmd control-port (string-append "RNTO " new-name) ftplib:pf-pair)))
	  final-resp))))

;;; delete the remote file
;;; returns a pair (#t . last-response) on success (#f . last-response)
;;; on error
(define (ftplib:delete-file control-port filename)
  (ftplib:ftp-cmd control-port (string-append "DELE " filename) ftplib:pf-pair))

;;; close the control connection
;;; returns a pair (#t . last-response) on success (#f . last-response)
;;; on error
(define (ftplib:close-control control-port)
  (ftplib:ftp-cmd control-port "QUIT" ftplib:pf-pair))

;;; close the data connection
;;; returns a pair (#t . last-response) on success (#f . last-response)
;;; on error
;;; NOTE:  shutdown is used here in an attempt to allow the socket to drain
(define (ftplib:close-data control-port data-port direction)
  (begin
    (if (eq? direction 'inbound) (shutdown data-port 1) (shutdown data-port 0))
    (ftplib:ftp-cmd control-port "" ftplib:pf-pair)))

;;; getfile returns (#t . last-resp) on success and (#f . last-resp) 
;;; on error
(define (ftplib:getfile control-port remote-filename output-port)
    (port-check? output-port? output-port (lambda () (ftplib:data-op control-port (string-append "RETR " remote-filename) (lambda (y x) (ftplib:copy-port (ftplib:determine-control-port y x)  output-port)))) 'output-port))

;;; putfile returns (#t . last-resp) on success and (#f . last-resp) 
;;; on error
;;; NOTE: an application program must check that the local-filename
;;;       is readable.
(define (ftplib:putfile control-port remote-filename input-port)
    (port-check? input-port? input-port (lambda () (ftplib:data-op control-port (string-append "STOR " remote-filename) (lambda (y x) (ftplib:copy-port input-port (ftplib:determine-control-port y x))))) 'input-port))

;;; ftplib:dir-list returns (#t . last-resp) on success and (#f
;;; . last-resp) on error 
(define (ftplib:dir-list control-port output-port . verbose)
    (port-check? output-port? output-port (lambda () (ftplib:data-op control-port (if (null? verbose) "NLST" "LIST") (lambda (y x) (ftplib:copy-port (ftplib:determine-control-port y x) output-port)))) 'output-port))

;;; verbose-ftplib:dir-list (ls -lrt)
(define (ftplib:verbose-dir-list control-port output-port)
  (ftplib:dir-list control-port output-port 'verbose))

;;; simple testing functions
;;; AKA "the documentation"
; (define my-ftp (ftplib:ftp "localhost" "anonymous" "fdfdrf"))
; (ftplib:set-transfer-mode 'passive)
; (ftplib:set-transfer-mode 'port)
; (ftplib:set-transfer-mode 'badmode)
; (ftplib:set-xfer-type my-ftp 'image)
; (ftplib:set-xfer-type my-ftp 'ascii)
; (ftplib:set-xfer-type my-ftp 'badtype)
; (ftplib:get-file-size my-ftp "/pub/junk2")
; (ftplib:get-file-size my-ftp "/pub/ddd")
; (ftplib:get-system-type my-ftp)
; (ftplib:get-file-modification-time my-ftp "/pub/junk2")
; (ftplib:get-file-modification-time my-ftp "/pub/ddd")
; (ftplib:make-directory my-ftp "/tmp/bad/bad/bad/bad")
; (ftplib:make-directory my-ftp "/incoming/goodd")
; (ftplib:make-directory my-ftp "/incoming/gooddir")
; (ftplib:change-directory my-ftp "/incoming/goodd")
; (ftplib:change-directory my-ftp "/incoming/badddd")
; (ftplib:parent-directory my-ftp)
; (ftplib:current-directory my-ftp)
; (ftplib:delete-directory my-ftp "/incoming/goodd")
; (ftplib:delete-directory my-ftp "/tmpest")
; (ftplib:rename-file my-ftp "/incoming/jj" "/incoming/brad")
; (ftplib:rename-file my-ftp "/incoming/jj" "/incoming/brad")
; (ftplib:rename-file my-ftp "/incoming/brad" "/incoming/jj")
; (ftplib:delete-file my-ftp "/incoming/huh")
; (ftplib:close-control my-ftp)


; (define my-ftp (ftplib:ftp "localhost" "anonymous" "fdfdrf"))
; (ftplib:set-transfer-mode 'passive)
; (ftplib:set-transfer-mode 'port)
; (ftplib:set-xfer-type my-ftp 'image)
; (ftplib:getfile my-ftp "/incoming/jj" (open-output-file "/tmp/junk"))
; (ftplib:getfile my-ftp "/pub/huh" (open-output-file "/tmp/junk"))
; (ftplib:putfile my-ftp "/incoming/junk28" (open-input-file "/tmp/junk"))
; (close my-ftp)
; (ftplib:copy-port (open-input-file "/var/lib/locatedb") (open-output-file "/tmp/dd1"))
; (ftplib:dir-list my-ftp (open-output-file "/tmp/k1"))
; (ftplib:verbose-dir-list my-ftp (open-output-file "/tmp/k1"))

