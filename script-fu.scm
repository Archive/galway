;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios O.                     ;;
;; jarios@usa.net                    ;;
;; http://erin.netpedia.net          ;;
;; Galway v0.2.1                     ;;
;; Diciembre 1998                    ;;
;; Junio 1999                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;soporte para script-fu
(define (script-fu str texto)
 (gtk-widget-realize texto)
 (gtk-text-insert texto #f "red" #f str -1)
 (gtk-text-thaw texto))

(define register-SF 
  "(script-fu-register
 \"script-fu-name\"                     ;;name of script
 \"<Toolbox>/Xtns/Script-Fu/\"          ;;location of script
 \" \"                                  ;;description of script
 \"Author:\" 		                    ;;author of script
 \"copyright 1999\"                     ;;copyright info
 \"1999\"                               ;;date
 \"    \"                               ;;type of images RGB, RGBA,
                                        ;; GRAY, GRAYA, INDEXED,
                                        ;;INDEXEDA          
                                        
;;Script parameteres should be place here

)")

(define sf-value "SF-VALUE \"TEXT:\" \" \" ")

(define sf-color "SF-COLOR \"Color:\" '(0 0 0)")

(define sf-toggle "SF-TOGGLE \"toggle?\" TRUE ")

(define sf-image "SF-IMAGE \"The image\" 0")

(define sf-drawable "SF-DRAWABLE \"The layer\" 0")



