;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Functions for working with the difr'ent pop menus.

;; (galway-text-button-connect xml)

;; Adds behaviour for showing the popupmenu:

(define (galway-text-button-connect xml)

  (define (buttonpress ev)
    (if (= (gdk-event-button ev) 3)
	(gtk-menu-popup (glade-xml-get-widget xml "menu1")  #f #f 0 1)))
  
  (gtk-widget-set-events (glade-xml-get-widget xml "text1") '(button-press-mask))
  (gtk-signal-connect (glade-xml-get-widget xml "text1") "button_press_event" buttonpress))



