;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; Released under GPL
;; Please Refer to the file COPYING

;; Functions for writting tagged text upon the GtkText widget

;; (galway-tags xml arg)
;; (galway-single xml arg)
;; (galway-write-str xml arg colour)
;; (galway-write-str-red xml arg)
;; (galway-write-str-blue xml arg colour)

;; This function writes tag of the following form:
;; <foo> </foo>

(define (galway-tags xml arg)
  (let ((text (glade-xml-get-widget xml "text1")))

    (gtk-widget-realize text)
    (gtk-text-insert text #f "red" #f 
		     (string-append "<" arg ">"
				    "</" arg ">")
		     -1)
    (gtk-text-thaw text)
    (gtk-widget-grab-focus text)))


;; This function write tags of the following form:
;; <foo>

(define (galway-single xml arg)
   (let ((text (glade-xml-get-widget xml "text1")))

     (gtk-widget-realize text)
     (gtk-text-insert text #f "red" #f 
		      (string-append "<" arg ">")
		      -1)
     (gtk-text-thaw text)
     (gtk-widget-grab-focus text)))

;; THe following set of functions write arg unto text
;; using the specifyed function. The first function
;; is the main wrapper for the other two.

(define (galway-write-str xml arg colour)  
  (let ((text (glade-xml-get-widget xml "text1")))
    (gtk-widget-realize text)
    (gtk-text-insert text #f colour #f arg -1)
    (gtk-text-thaw text)
    (gtk-widget-grab-focus text)))

(define (galway-write-str-red xml arg) (galway-write-str xml arg "red"))

(define (galway-write-str-blue xml arg) (galway-write-str xml arg "blue"))





