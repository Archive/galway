;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; ariel@arcavia.com        ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Signal definitions

;; (galway-notebook-signal)

(define-public (galway-notebook-signal xml)
  (let ((notebook (glade-xml-get-widget xml "notebook1")))
    (gtk-signal-connect notebook "switch_page"
			(lambda (col num)
			  (galway-dialog-put-into-buffer xml galway-document-index)
			  (galway-parse-file xml galway-document-index)))))
  










