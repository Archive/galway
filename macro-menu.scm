;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Function for appending items unto a menubar and connecting signals unto them.

;; (galway-define-menu menu-name menubar xml)
;; (galway-menu-html-top-level xml menu)
;; (galway-menu-html-head xml menu)
;; (galway-menu-html-block-level xml menu)
;; (galway-menu-html-special-inline xml menu)
;; (galway-menu-html-other xml menu)
;; (galway-menu-html-font-style xml menu)
;; (galway-menu-html-frames xml menu)
;; (galway-menu-html-various xml menu)
;; (galway-menu-vrml-bindable xml menu)
;; (galway-menu-vrml-common xml menu)
;; (galway-menu-vrml-geometry xml menu)
;; (galway-menu-vrml-group xml menu)
;; (galway-menu-vrml-interpolator xml menu)
;; (galway-menu-vrml-properties xml menu)	
;; (galway-menu-vrml-sensors xml menu)
;; (galway-menu-vrml-other xml menu)
;; (galway-menu-jscript-statements xml menu)
;; (galway-menu-jscript-funcs xml menu)
;; (galway-menu-script-fu-types xml menu)
;; (galway-menu-script-fu-scheme xml menu)
;; (list-n-elements obj n)

(includer "string.scm"  "vrml.scm"
	  "jscript.scm" "jscript-funcs.scm"
	  "script-fu.scm")

;; We are goin to receive the xml, the menu unto we 
;; are going to append the new items, the list of 
;; new names and list of functions.

;; Please be aware that all the functions listed in func
;; should be of the form
;; (function xml rgar)
;; Internally each function should deal with proper management of widgets
;; And of arguments. Obviously, arg can have any type (list, pair, number, vector, etc) 
;; We do the for cleanlyness and because it's easier to work this way.
 
(define (galway-define-menu xml menubar ls func arg) 

	(letrec ((schemer 
		  (lambda (ls func arg)
		    (if (pair? ls)

			(let ((item (gtk-menu-item-new-with-label (car ls))))

			  (gtk-menu-append menubar item)

			  (gtk-signal-connect item "activate"
					      (lambda ()
						((car func) xml (car arg))))
			  (schemer (cdr ls) (cdr func)(cdr arg)))))))
	  
	  (schemer ls func arg)))
			
;; Functions for adding items
;; This was done with macros but no longer.
;; We now declare normal functions because
;; macros are slower than normal functions.
;; This functions used to be declared in menu.scm
;; but it'll be deprecated.

;; Be aware that this functions only has xml and menu as argument
;; The rest are pretty declared inside since we know they are static.

;; HTML menu functions

(define (galway-menu-html-top-level xml menu)
  (galway-define-menu xml menu
		      (list "HTML""HEAD" "BODY" "FRAMESET")
		      (list-n-elements galway-tags 4)
		      (list "HTML""HEAD" "BODY" "FRAMESET")))
		       
(define (galway-menu-html-head xml menu)
  (galway-define-menu xml menu
		      (list "BASE" "ISINDEX" "LINK" "META" "SCRIPT" "STYLE" "TITLE")
		      (list-n-elements galway-single 7)
		      (list  "BASE HREF=\"\" " 
			     "ISINDEX"  
			     "LINK" 
			     "META NAME=\"\" HTTP-EQUIV=\"\" CONTENT=\"\" " 
			     "SCRIPT" 
			     "STYLE" 
			     "TITLE")))

(define (galway-menu-html-block-level xml menu)
  (galway-define-menu xml menu
		(list "ADDRESS" "BLOCKQUOTE" "CENTER" "DEL" "DIV" 
		      "H1" "H2" "H3" "H4" "H5" "H6"
		      "HR" "INS" "ISINDEX" "NOSCRIPT" "P" "PRE")
		(list-n-elements galway-tags 17)
		(list "ADDRESS" "BLOCKQUOTE" "CENTER" "DEL" "DIV" 
		      "H1" "H2" "H3" "H4" "H5" "H6"
		      "HR" "INS" "ISINDEX" "NOSCRIPT" "P" "PRE")))
			       
(define	(galway-menu-html-special-inline xml menu)
  (galway-define-menu xml menu 
		      (list "A" "APPLET" "BASEFONT" "BDO" "BR" "FONT" 
			    "IFRAME" "IMG" "MAP" "AREA" "OBJECT"
			    "PARAM" "Q" "SCRIPT" "SPAN" "SUB" "SUP")
		      (list galway-tags galway-tags galway-single galway-tags 
			    galway-single galway-tags galway-tags galway-single 
			    galway-tags galway-single galway-tags galway-tags 
			    galway-tags galway-tags galway-tags galway-tags galway-tags )
		      (list "A" "APPLET" "BASEFONT" "BDO" "BR" "FONT" 
			    "IFRAME" "IMG" "MAP" "AREA" "OBJECT"
			    "PARAM" "Q" "SCRIPT" "SPAN" "SUB" "SUP")))

(define (galway-menu-html-other xml menu)
  (galway-define-menu xml menu
		      (list "ABR" "ACRONYM" "CITE" "CODE" "DEL" "DFN" 
			    "EM" "INS" "KBD" "SAMP" "STRONG" "VAR")		      
		      (list-n-elements galway-tags 12)
		      (list "ABR" "ACRONYM" "CITE" "CODE" "DEL" "DFN" 
			    "EM" "INS" "KBD" "SAMP" "STRONG" "VAR")))

(define (galway-menu-html-font-style xml menu)
  (galway-define-menu xml menu
		      (list "B" "BIG" "I" "S" "SMALL" "STRIKE" "TT" "U")
		      (list-n-elements galway-tags 8)
		      (list "B" "BIG" "I" "S" "SMALL" "STRIKE" "TT" "U")))

;(define (galway-menu-html-forms xml menu))

(define (galway-menu-html-frames xml menu)
  (galway-define-menu xml menu
		      (list "FRAMESET" "FRAME" "NOFRAMES")
		      (list galway-tags galway-single galway-tags)
		      (list "FRAMESET" "FRAME" "NOFRAMES")))

(define (galway-menu-html-various xml menu)
  (galway-define-menu xml menu
		      (list "Image tag"     "Image Dialog" 
			    "Ordered Lists" "Unordered lists"
                            "Links"         "Tables")
		      (list galway-single
			    galway-dialog-html-image
			    galway-dialog-html-list
			    galway-dialog-html-list
			    galway-dialog-html-link
			    galway-dialog-html-table)
		      (list "IMG SRC=\" \""
			    #f #t #f #f #f)

			    ;;FIXME
			    ;; "Body" "Frames" "Forms" 
		      ))

;(define (galway-menu-html-various xml menu))


;; VRML menu functions

(define (galway-menu-vrml-bindable xml menu)
  (galway-define-menu xml menu
		      (list "background" "fog" "navigationinfo" "viewpoint")
		      (list-n-elements galway-write-str-blue 4)
		      (list background fog navigationinfo viewpoint)))

(define (galway-menu-vrml-common xml menu)
  (galway-define-menu xml menu
		      (list "Audio" "Directional Light" "Point Light" 
			    "Script" "Sound" "Spot Light" "World Info")    
		      (list-n-elements galway-write-str-blue 7)
		      (list audioclip direclight pointlight
			    skript sound spotlight worldinfo)))

(define (galway-menu-vrml-geometry xml menu)
  (galway-define-menu xml menu
		      (list "Box" "Cone" "Elevation Grid" "Extrusion" 
			    "Indexed Face Set" "Indexed Line Set" "Point Set" 
			    "Sphere" )
		      (list-n-elements galway-write-str-blue 8)
		      (list box cone elevgrid extrusion indexedfaceset
			   indexedlineset pointset sphere)))

(define (galway-menu-vrml-group xml menu)
  (galway-define-menu xml menu
		      (list "Anchor" "Billboard" "Collision" "Group" "Inline"
			    "Transform" "LOD" "Switch")
		      (list-n-elements galway-write-str-blue 8)
		      (list anchor billboard collision group inline
			    transform lod switch)))

(define (galway-menu-vrml-interpolator xml menu)
  (galway-define-menu xml menu
		      (list "Color" "Coordinate" "Normal" "Orientation" "Position" "Scalar")
		      (list-n-elements galway-write-str-blue 6)
		      (list  colorinter coordinateinter normalinter
			     orientationinter positioninter scalarinter)))

(define (galway-menu-vrml-properties xml menu)
  (galway-define-menu xml menu
		      (list "Appearance" "Color" "Coordinate" "Font Style" "Image Texture"
			    "Material" "Movie Texture" "Normal" "Pixel" "Texture Coordinate"
			    "Texture Transform")
		      (list-n-elements galway-write-str-blue 11)
		      (list appearance color coordinate fontstyle
			    imagetexture material movietexture 
			    normal pixeltexture texturecoordinate
			    texturetransform)))

(define (galway-menu-vrml-sensors xml menu)
  (galway-define-menu xml menu
		      (list "Cylinder" "Plane" "Proximity" "Time" "Touch" "Visibility")
		      (list-n-elements galway-write-str-blue 6)
		      (list cylindersensor planesensor proximitysensor
			    timesensor touchsensor visibilitysensor)))


(define (galway-menu-vrml-other xml menu)
  (galway-define-menu xml menu
		      (list "Comment" "Header 2.0" "Header '97")
		      (list-n-elements galway-write-str-blue 3)
		      (list vrmlcomment header2.0 header97)))

;; Jscript menu functions

(define (galway-menu-jscript-statements xml menu)
  (galway-define-menu xml menu
		       (list "function" "Break" "Continue" "If" 
			     "For" "Return" "While" "With" "Var")
		       (list-n-elements galway-write-str-red 9)
		       (list Sfunc Sbreak Scontinue Sif Sfor
			     Sreturn Swhile Swith Svar)))

(define (galway-menu-jscript-funcs xml menu)
  (galway-define-menu xml menu
		      (list "Scrolling Status Bar" "Scrolling Stauts Bar 2"
			    "On mouse over" "VRML rotate animaton")
		      (list galway-write-str-red galway-write-str-red 
			    galway-single galway-write-str-red )
		      (list jscript:tt-statusbar-text
			    jscript:tt-statusbar-text
			    jscript:onmouse
			    jscript:vrml-rotation)))
			    
;; Script-fu menu functions

(define (galway-menu-script-fu-types xml menu)
  (galway-define-menu xml menu
		      (list "SF-VALUE" "SF-COLOR" "SF-TOGGLE" "SF-IMAGE" "SF-DRAWABLE")
		      (list-n-elements galway-write-str-red 5)
		      (list sf-value sf-color sf-toggle sf-image sf-drawable)))

(define (galway-menu-script-fu-scheme xml menu)
  (galway-define-menu xml menu
		      (list "(and)" "(append)" "(apply)" "(begin)" "(car)" "(cdr)"
			    "(cons)" "(equal?)" "(if)" "(lambda)" "(let)" "(let*)"
			    "(letrec)")
		       (list-n-elements galway-write-str-red 13)
		       (list "(and exp ...)" 
			     "(append list ...)"
			     "(apply procedure obj ... list)"
			     "(begin exp1 exp2 ...)"
			     "(car pair)"
			     "(cdr pair)"
			     "(cons obj1 obj2)"
			     "(equal? obj1 obj2)"
			     "(if test consequent alternative)"
			     "(lambda formals exp1 exp2 ...)"
			     "(let name ((var val) ...) exp1 exp2 ...)"
			     "(let* ((var val) ...) exp1 exp2 ...)"
			     "(letrec ((var val) ...) exp1 exp2 ...)" )))
		      

;; This function should go elsewhere
;; Maybe it should be coded in C....

(define (list-n-elements obj n)
  (if (zero? n)
      '()
      (cons obj (list-n-elements obj (1- n)))))





