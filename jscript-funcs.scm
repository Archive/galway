;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

(define jscript:vrml-rotation
"field SFFloat angle 0
field SFFloat dang 0.392

eventin SFFloat startScript

eventOut SFRotation position_changed

function startScript(st){
 angle += dang;
 if(angle >= 3.141592654)
   angle -= 3.141592654;
 position_changed[0] = 0;
 position_changed[1] = 1;
 position_changed[2] = 0;
 position_changed[3] = angle; }

#ROUTE sensor.touchTime TO time.startTime
#ROUTE tiempo.fraction_changed TO rotation.startScript
#ROUTE rotation.position_changed TO figure.set_rotation ")

(define jscript:tt-statusbar-text
"<BODY  onLoad=\"snapSetup()\">
<SCRIPT LANGUAGE=\"JavaScript\">
<!--
function snapIn(jumpSpaces,position){
  var msg=\"Your text goes here!!!\"
  var out = \"\"

  if (killScroll) { return false }

  for (var i = 0; i < position; i++){ 
   out += msg.charAt(i) }

  for (i = 1; i < jumpSpaces; i++){ 
   out += \" \" }

  out += msg.charAt(position)

  window.status = out

  if (jumpSpaces <= 1){
    position++
    if (msg.charAt(position) == ' '){
     position++ }

    jumpSpaces = 100-position }

  else if (jumpSpaces > 3) {
  jumpSpaces *= .75 }

  else {jumpSpaces-- }

  if (position != msg.length){
   var cmd = \"snapIn(\" + jumpSpaces + \",\" + position + \")\";
   scrollID = window.setTimeout(cmd,5); }
  else {
    scrolling = false
    return false }
  return true }

function snapSetup(){

  if (scrolling)
   if (!confirm('Re-initialize snapIn?'))
      return false
  killScroll = true
  scrolling = true

  var killID = window.setTimeout('killScroll=false',6)
  scrollID = window.setTimeout('snapIn(100,0)',10)
  return true }

var scrollID = Object
var scrolling = false
var killScroll = false

//-------------------------------------------------------------

// -->

</SCRIPT>")

(define jscript:onmouse 
"A HREF=\"URL\" onMouseOver\"window.status='Your Message goes Here';
return true\">Name of link goes here! </A")








