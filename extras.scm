;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; jarios@usa.net           ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Functions that add extra functionality to
;; scheme and can't be placed elsewhere

;; (galway-list-add ls obj)
;; (galway-string-null? str)
;; (list-n-elements obj n)
;; (galway-last-char? str char)
;; (galway-file-from-path-to-file str)
;; (galway-string->ls str)
;; (galway-member ls obj)

;; Adds a new element to the list and returns it.

(define (galway-list-add ls obj) (reverse (cons obj (reverse ls))))

;; Checks if str is epsilon.

(define (galway-string-null? str) (not (zero? (string-length str))))


;; Makes a list of n numbers of obj

(define (list-n-elements obj n) (if (zero? n) '() (cons obj (list-n-elements obj (1- n)))))

;; Checks if last char of string is equal to char.

(define (galway-last-char? str char) (eqv? char (car (reverse (string->list str)))))
 
;; Receives a complete file path and then returns only the file name...

(define (galway-file-from-path-to-file str)

  (letrec((schemer
	   (lambda(ls final)
	     (cond
	      ((null? ls) final)
	      ((equal? #\/ (car ls)) final)
	      (else (schemer (cdr ls) (cons (car ls) final)))))))
    
    (list->string (schemer (reverse (string->list str)) '()))))

;; Receives a string and returns a list of the words ;)

(define (galway-string->ls str)

 (letrec ((lister             
            (lambda (str ls)

	      (if (zero? (string-length str))
		  ls
		  (lister (galway-word-cdr str) 

			  (if (list? (galway-word-car str))
			      ls
			      (galway-list-add ls (galway-word-car str))))))))
    (lister str '())))


(define (galway-word-car str)
 (let ((ls1 (string->list str)))
	
      (letrec ((word-car 
                (lambda (ls)
		 
                 (cond
		   ((not (pair? ls)) '())
		   ((equal? (car ls) #\space )'() )
		   ((equal? (car ls) #\newline) '() )
		   ((equal? (car ls) #\tab) '() )
                   (else (cons (if (zero? (string-length (string (car ls))))
				   '() (car ls))
                               (word-car (cdr ls))))) )))
      (let ((ls (word-car ls1)))
	(if (zero? (length ls))
	    '()
	    (list->string ls))))))

(define (galway-word-cdr str)

 (let ((ls (string->list str)))

       (letrec ((word-cdr
                 (lambda (ls)
                  (cond
                   ((not (pair? ls)) '())
                   ((equal? (car ls) #\space ) (cdr ls))  
		   ((equal? (car ls) #\tab) (cdr ls))
		   ((equal? (car ls) #\newline) (cdr ls))
                   (else (word-cdr (cdr ls)))))))
               (list->string (word-cdr ls))) ))

(define (galway-list-member ls obj)

  (letrec ((member
	    (lambda (ls n)
	      (cond 
	       ((null? ls) #f)
	       ((equal? (car ls) obj) n)
	       (else
		(member (cdr ls) (1+ n)))))))

    (member ls 0)))





