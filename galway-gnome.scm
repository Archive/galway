;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios O.                     ;;
;; ariel@arcavia.com                 ;;
;; http://erin.netpedia.net          ;;
;; Mi na Nollag 15 1998              ;;
;; Feahbra 10 2001                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; This program is part of the GNU project, released
;; under the aegis of GNU and released under the GPL

(define-module (galway galway-gnome)
  :use-modules (gnome gnome)
  :use-modules (gtk gtk)
  :use-modules (gtk gdk)
  :use-modules (gtk libglade)
  :use-modules (gtk gtkhtml))
    

;;(debug-enable 'debug)

(define galway-start-dir "./")

;; The main galway-gnome file.
;; All the magic will be done here.
;; This file should be as small as possible

(define html (gtk-html-new))
(define script-xml #f)

(define galway-document-index #f)
(define galway-document-file-name '())
(define galway-document-buffer '())
(define galway-document-perm '())

(load "macro.scm")

(define-public (galway-gnome)

  (gdk-rgb-init)

  (gtk-widget-set-default-colormap (gdk-rgb-get-cmap))
  (gtk-widget-set-default-visual (gdk-rgb-get-visual))
  
  (glade-gnome-init)

  (gnome-init-hack "galway-gnome" #f '())

  
  ;; We only need to have binded xml and window.
  ;; We are not going to bind every single widget.

  (let* ((xml (glade-xml-new xml-file))
	 (window (glade-xml-get-widget xml "window1")))
    
    ;; Before anything is wrong we check if we have a .galwayrc file:

    (set! script-xml xml)

    (if (galway-check-rc xml)
	(begin (gtk-widget-show (glade-xml-get-widget xml "messagebox1"))
	       (include "files.scm"))
	(gtk-widget-show (glade-xml-get-widget xml "preferences2")))

   ;;(include "files.scm")

    (galway-plugin-available xml #f)
    (galway-plugin-load xml galway-plugin-list)

    ;; We'll try to have most signals defined in the xml file

    (glade-xml-signal-autoconnect xml)

    ;;This will contain all the signals to be defined.
    ;;We pass unto the function the xml so we can lately call
    ;;the required widgets.
    ;;Hopefully, only the signals.scm will load all the signals
    
    (galway-define-menu-all xml)

   (galway-dialog-file-signal xml)
   (galway-ctree-dir-signal xml)
   (galway-dialog-find-all-signal xml)
   (galway-dialog-preferences-signal xml)
   (galway-menu-item-all-signal xml)
   (galway-scripting-signal xml)
   (galway-toolbar-button-signal xml)
   (galway-notebook-signal xml)

   (galway-text-button-connect xml)

    ;; We need to call these so the opened directory is opened.

    (galway-ctree-dir-add-items xml galway-start-dir)

    ;; libglade does not support GtkHTML so this is how we include it
    ;; in our gui:

    (gtk-container-add (glade-xml-get-widget xml "scrolledwindow10") html)

    (gtk-widget-grab-focus (glade-xml-get-widget xml "text1"))

    ;; When we open a file we need to have one document loaded
    
    (galway-document-new xml 0)

    (gtk-widget-show-all window)
    
    (gtk-standalone-main window) ))







