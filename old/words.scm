;;;;;;;;;;;;;;;;;;;;;;;
;;Ariel Rios         ;;
;;jarios@usa.net     ;;
;;                   ;;
;;                   ;;
;;;;;;;;;;;;;;;;;;;;;;;

;;The following 2 functions were taken from my "Theory
;;of Computation" final project "Scheme Parser" =)

(define (word-car str)
 (let ((ls1 (string->list str)))
	;(display ls)(newline)
      (letrec ((recorre 
                (lambda (ls)
		  ;(display ls)
                 (cond
		   ((not (pair? ls)) '())
		   ((equal? (car ls) #\space )'() )
		   ((equal? (car ls) #\newline) '() )
		   ((equal? (car ls) #\tab) '() )
                   (else (cons (if (zero? (string-length (string (car ls))))
				   '() (car ls))
                               (recorre (cdr ls))))) )))
      (let ((ls (recorre ls1)))
	(if (zero? (length ls))
	    '()
	    (list->string ls))))))

(define (word-cdr str)
 (let ((ls (string->list str)))
       (letrec ((recorre
                 (lambda (ls)
                  (cond
                   ((not (pair? ls)) '())
                   ((equal? (car ls) #\space ) (cdr ls))  
		   ((equal? (car ls) #\tab) (cdr ls))
		   ((equal? (car ls) #\newline) (cdr ls))
		   ;((equal? (string (car ls)) (or "\n" "\t" " " "")) (cdr ls))
                   (else (recorre (cdr ls)))))))
               (list->string (recorre ls))) ))

(define (find word str)
 (let ((m -1)
       (i (string-length str)))
  (do ((n 0 (+ n 1)))
      ((>= n i))
      (begin
        (if (equal? (string-ref word 0)
                    (string-ref str n))
            (if (equal? word (word-car (substring str n i)))
                (begin 
                 (set! m n)
                 (set! n i)) ))))
        m ))  


;(define (find word str)
;  (letrec ((find 
;	    (lambda(str n)
;	      (cond
;	       ((< (string-length str) 1) -1)
;	       ((equal? word (word-car str)) (1+ n))
;	       (else 
;		(find (word-cdr str) (+ n (if (> string-length (word-car str))
					      ;(string-length (word-car str))
					      ;1))))))))
   ; (find str 0)))

(define (list-del ls k)
 (letrec ((delete 
           (lambda (ls n)
            (cond 
             ((not (pair? ls))'())
             ((equal? k n)(delete (cdr ls) (+ n 1)))
             (else (cons (car ls) (delete (cdr ls)(+ n 1))))))))
          (delete ls 0)))

(define (list-add-member ls k)
 (let* ((n (length ls))
        (tmp(make-list (+ n 1) 0)) )
       (do
          ((i 0 (+ i 1)))
          ((= i n))
          (list-set! tmp i (list-ref ls i)))
       (list-set! tmp n k)
       tmp))

(define (push ls obj)
(set! ls (cons ls obj)))


;;Originally created by Ariel Rios 
;;for gnumeric-guile numerical methods function library
;;Licensed under LGPL
;;July 13, 1999

;;Modified for use in the galway web editor


(define (str->ls str)      ;;makes a list of the words of the 
 (letrec ((lister              ;;string
            (lambda (str ls)
	      (if (zero? (string-length str))
		  ls
		  (lister (word-cdr str) 
			  (if (list? (word-car str))
			      ls
			      (list-add-member ls (word-car str))))))))
    (lister str '())))

(define (last-char? str char)
  (equal? char (car (reverse (string->list str)))))














