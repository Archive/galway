;;;;;;;;;;against this constants I will do the ands & the xors...

(define S_IFDIR 16384)
(define S_IREAD   256)
(define S_IWRITE  128)
(define S_IEXEC    64)
(define S_IRGRP    32)
(define S_IWGRP    16)
(define S_IXGRP     8)
(define S_IROTH     4)
(define S_IWOTH     2)
(define S_IXOTH     1)

;;Since all the comparisions are the same I found
;;the following macro very usefull

(define-macro (define-perm? name constant)
  `(define (,name mode)
     (if (zero? (logand mode ,constant))
	 #f 
	 #t)))

(define-perm? owner-read? S_IREAD)
(define-perm? owner-write? S_IWRITE)
(define-perm? owner-exec? S_IEXEC)
(define-perm? group-read? S_IRGRP)
(define-perm? group-write? S_IWGRP)
(define-perm? group-exec? S_IXGRP)
(define-perm? univ-read? S_IROTH)
(define-perm? univ-write? S_IWOTH)
(define-perm? univ-exec? S_IXOTH)

;;OK. We get the list of buttons.
;;We get the name of the file by 
;;a list-ref. THen we do a super 
;;stat and we set the button accordingly

(define (galway-butons-set! ls)
  (let* ((file (list-ref name-ls doc-index))
	 (ststat (stat file))
	 (file-mode (stat:mode ststat)))
    (letrec ((buttonator
	      (lambda (ls-button ls-perm)
		(cond 
		 ((null? ls-button) '())
		 (else  (gtk-toggle-button-set-active (car ls-button) ((car ls-perm) file-mode) )
			(buttonator (cdr ls-button ls-perm)))))))
      (buttonator ls '(owner-read? owner-write? owner-exec? 
		       group-read? group-write? group-exec?
                       univ-read?  univ-write?  univ-exec?)))))	   

;;Well the upper funcions should be the real thing...
;;However at this time I will use the following and later on
;;I will change merge both functions...
;;This function REALLY SUCKS!!!!      

(define (galway-buttons-set-permissions mode)
  (let ((oRead  (car ownerP))   (oWrite (cadr ownerP))  (oExec  (caddr ownerP))
       (gRead  (car groupP))    (gWrite (cadr groupP))  (gExec  (caddr groupP))
       (uRead  (car univP))     (uExec  (cadr univP))   (uWrite (caddr univP)))
   (if (owner-read? mode)
       (gtk-toggle-button-set-state oRead #t)
       (gtk-toggle-button-set-state oRead #f))
   (if (owner-write? mode)
       (gtk-toggle-button-set-state oWrite #t)
       (gtk-toggle-button-set-state oWrite #f))
   (if (owner-exec? mode)
       (gtk-toggle-button-set-state oExec #t)
       (gtk-toggle-button-set-state oExec #f))
   (if (group-read? mode)
       (gtk-toggle-button-set-state gRead #t)
       (gtk-toggle-button-set-state gRead #f))
   (if (group-write? mode)
       (gtk-toggle-button-set-state gWrite #t)
       (gtk-toggle-button-set-state gWrite #f))
   (if (group-write? mode)
       (gtk-toggle-button-set-state gExec #t)
       (gtk-toggle-button-set-state gExec #f))
   (if (univ-read? mode)
       (gtk-toggle-button-set-state uRead #t)
       (gtk-toggle-button-set-state uRead #f))
   (if (univ-write? mode)
       (gtk-toggle-button-set-state uWrite #t)
       (gtk-toggle-button-set-state uWrite #f))
   (if (univ-exec? mode)
       (gtk-toggle-button-set-state uExec #t)
       (gtk-toggle-button-set-state uExec #f))))

;;Ok. We need a function that let us change the permissions

(define (set-permission permission)
  (let* ((file (list-ref name-ls doc-index))
	 (old-mode (stat:mode (stat file)))
	 (new-mode (number->string (logxor old-mode permission)))
	 (command (string-append "chmod " new-mode " " file))) 
  (system command)))  ;;This system call should be changed in the future...

;;This macro will define changing permissions for every button.

(define-macro (define-button-set-file-permission name permission)
  `(define (,name)
     (set-permission ,permission)))

(define-button-set-file-permission owner-read S_IREAD)
(define-button-set-file-permission owner-write S_IWRITE)
(define-button-set-file-permission owner-exec S_IEXEC)
(define-button-set-file-permission group-read S_IRGRP)
(define-button-set-file-permission group-write S_IWGRP)
(define-button-set-file-permission group-exec S_IXGRP)
(define-button-set-file-permission univ-read S_IROTH)
(define-button-set-file-permission univ-write S_IWOTH)
(define-button-set-file-permission univ-exec S_IXOTH)

(define (connect-permissions-buttons ls-button ls-mode)  
  (cond
   ((null? ls-button) #t)
   (else (gtk-signal-connect (car ls-button) "clicked"
			     (lambda()
			       (eval(car ls-mode))))
	 (connect-permissions-buttons (cdr ls-button) (cdr ls-mode))))) 

(define (start-connecting)
  (let ((oRead  (car ownerP))   (oWrite (cadr ownerP))  (oExec  (caddr ownerP))
	(gRead  (car groupP))    (gWrite (cadr groupP))  (gExec  (caddr groupP))
	(uRead  (car univP))     (uExec  (cadr univP))   (uWrite (caddr univP)))
    (connect-permissions-buttons (list oRead oWrite oExec gRead gWrite gExec uRead uWrite uExec)
				 (list '(owner-read) '(owner-write) '(owner-exec)
				       '(group-read) '(group-write) '(group-exec)
				       '(univ-read) '(univ-write) '(univ-exec))) ) )
  
    
  





