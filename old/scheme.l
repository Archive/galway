%{
/*
Ariel Rios Osorio
jarios@usa.net
http://erin.netpedia.net
Licensed under GPL

These are scheme lexer humble begginings!
*/

#include <libguile.h>
#include "scheme.yy.tab.h"

int boolCount = 0;
int defCount = 0;
int guileCount = 0;
int primCount = 0; 
int oPar = 0; 
int cPar = 0;
int other = 0;
int stack = 0;
%}
%%

[\t\n ]+ 

abs | 
acos | 
angle | 
append | 
apply |
asin |
assoc |
assq |
assv |
atan |
boolean? | 
caar |
cadr |
call-with-current-continuation |
call-with-input-file |
call-with-output-file |
call-with-values |
call/cc |
car |
catch |
cdddar |
cddddr |
cdr |
ceiling | 
char->integer | 
char-alphabetic? | 
char-ci<=? |
char-ci=? |
char-ci>=? |
char-ci>? |
char-ci<? |
char-downcase |
char-lower-case? |
char-numeric? |
char-ready? |
char-upcase |
char-upper-case? | 
char-whitespace? |
char<=? |
char<? |
char=? |
char>=? |
char>? |
char? |
close-input-port | 
close-output-port |
complex? |
cons |
cos |
current-input-port |
current-output-port | 
denominator |
display |
dynamic-wind | 
eof-object? |
eq? |
equal? | 
eqv? |
error |
eval |
even? |
exact->inexact | 
exact? |
exp |
expt |
floor |
for-each | 
force |
gcd |
imag-part |
inexact->exact |
inexact? |
input-port? | 
integer->char | 
integer? |
lcm |
length | 
list |
list->string | 
list->vector |
list-ref |
list-tail |
list? |
load |
log |
magnitude | 
make-polar |
make-rectangular | 
make-string |
make-vector |
map |
max |
member | 
memq |
memv |
min |
modulo |
negative? |
newline |
not |
null? |
number->string |
number? |
numerator | 
odd? |
open-input-file | 
open-output-file |
output-port? |
pair? |
peek-char |
port? |
positive? | 
procedure? |
promise |
quotient |
rational? |
rationalize | 
read |
read-char | 
real-part |
real? |
remainder | 
reverse |
round |
sin |
sqrt |
string | 
string->list | 
string->number | 
string->symbol |
string-append |
string-ci<=? |
string-ci<? |
string-ci=? |
string-ci>=? |
string-ci>? |
string-copy |
string-fill! | 
string-length |
string-ref |
string-set! |
string<=? |
string<? |
string=? |
string>=? |
string>? |
string?  |
substring |
symbol->string? | 
symbol? |
tan |
transcript-off | 
transcript-on |
truncate |
vector |
vector->list |
vector-fill! |
vector-length |
vector-ref |
vector-set! |
vector? |
with-input-from-file |
with-output-to-file |
write |
write-char | 
zero?  { primCount++; }

and |
case |
cond |
define |
define-syntax |
delay | 
do |
else |
if |
lambda |
let |
let* |
let-syntax |
letrec |
letrec-syntax { defCount++; }

basename |
chdir |
chown |
chmod |
close |
closedir |
copy-file |
delete-file |
dir-name |
fcntl |
fsync |
get-cwd |
link |
list-set! |
lstat |
merge |
merge! |
mkdir |
open |
open-fdes |
opendir |
readdir |
readlink |
read-only-string? |
rename-file |
rewinddir |
rmdir |
scm-error |
select |
socket |
sort |
sort! |
sort-list |
sort-list! |
sorted? |
stable-sort |
stable-sort! |
stat |
strerror |
string-index |
string-null? |
string-rindex |
string-prefix-predicate |
string-prefix=? |
string-downcase !
string-upcase! |
substring-move-left! |
substring-move-right! |
substring-fill! |
symlink |
system |
truncate-file |
umask { guileCount++; }

#[tf] { boolCount++; }

#\[.] //caracter

;[.]+ //Comentario

[(]         { oPar++; stack++;}

[)]          { cPar++; stack--;}

[a-zA-Z]+    { other++;}

[1-9]+       { other++;}

.            { other++;}

%%








