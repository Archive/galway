%{
/*
Ariel Rios
jarios@usa.net
http://erin.netpedia.net
*/

//Licensed under the GPL
%}

%%

%%

void main(int argc, char **argv){
extern FILE * yyin;
if(argc > 1){
FILE * file;
file = fopen(argv[1], "r");
yyin = file;
yyparse();
else exit(1);
}

void yyerror(char *s){
printf("%s\n", s);
}	




